<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\front\profile\ProfileRequest;
use App\Http\Requests\front\profile\SocialRequest;
use App\Http\Requests\front\profile\AddressRequest;
use Storage;
use File;

use App\Models\User;
use App\Models\Address;
use App\Models\LogUser;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show() 
    {
        $user = User::with('members', 'members.brand','address', 'deliAddress')->find(auth()->user()->id);

        return response()->json($user, 200);
    }

    public function updateProfile(ProfileRequest $request, $id) 
    {
        $user = User::findOrFail($id);
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->nickname = $request->get('nickname');
        $user->email = $request->get('email');
        $user->gender = $request->get('gender');
        $user->age = $request->get('age');
        $user->phone = $request->get('phone');

        if($request['img']){
            $file_old = $user->img;
            Storage::disk('storage_global')->delete($file_old);

            $image = $request['img'];
            $extension = $image->getClientOriginalExtension();
            $name = 'users/'.$user->id.'/'.'users-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $user->img = $name;
        }

        $user->save();

        return response()->json($user, 200);
    }

    public function updateSocial(SocialRequest $request, $id) 
    {
        $user = User::findOrFail($id);
        $user->line = $request->get('line');
        $user->facebook = $request->get('facebook');
        $user->instragram = $request->get('instragram');

        if($request['qrcode_line_img']){
            $file_old = $user->qrcode_line_img;
            Storage::disk('storage_global')->delete($file_old);

            $image = $request['qrcode_line_img'];
            $extension = $image->getClientOriginalExtension();
            $name = 'users/'.$user->id.'/'.'lineimg-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $user->qrcode_line_img = $name;
        }

        $user->save();

        return response()->json($user, 200);
    }

    public function updateAddress(AddressRequest $request, $id) 
    {
        $addressId     = $request->get('address_id');
        $addressDeliId = $request->get('deli_address_id');

        $address = Address::findOrFail($addressId);
        $address->address = $request->get('add_address');
        $address->subdistrict = $request->get('add_subdistrict');
        $address->district = $request->get('add_district');
        $address->province = $request->get('add_province');
        $address->zipcode = $request->get('add_zipcode');
        $address->save();

        $Deliaddress = Address::findOrFail($addressDeliId);
        $Deliaddress->address = $request->get('deli_address');
        $Deliaddress->subdistrict = $request->get('deli_subdistrict');
        $Deliaddress->district = $request->get('deli_district');
        $Deliaddress->province = $request->get('deli_province');
        $Deliaddress->zipcode = $request->get('deli_zipcode');
        $Deliaddress->save();

        $user = User::with('address', 'deliAddress')->find(auth()->user()->id);

        return response()->json($user, 200);
    }
}
