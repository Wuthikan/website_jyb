<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Response;

class DisplayImageController extends Controller
{

    public function imagePublic($database, $id, $filename)
    {
        $name = $database.'/'.$id.'/'.$filename;
        $exists = Storage::disk('public')->has($name);

        if (!$exists) {
            abort(404);
        }

        return $this->getImage($name,'public');
    }

    public function imageGlobal($database, $id, $filename)
    {
        $name = $database.'/'.$id.'/'.$filename;
        $exists = Storage::disk('storage_global')->has($name);

        if (!$exists) {
            abort(404);
        }

        return $this->getImage($name,'storage_global');
    }

    public function imagePrivate($database, $id, $filename)
    {
        $name = $database.'/'.$id.'/'.$filename;
        $exists = Storage::disk('storage_private')->has($name);

        if (!$exists) {
            abort(404);
        }

        return $this->getImage($name,'storage_private');
    }

    private function getImage($filename, $storage)
    {
        $file = Storage::disk($storage)->get($filename);
        $type = Storage::disk($storage)->mimeType($filename);
        $response = Response::make($file, 200);
        $response->header("Content-Type",$type );
        return $response;
    }
}
