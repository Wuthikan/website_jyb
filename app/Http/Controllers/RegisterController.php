<?php

namespace App\Http\Controllers;
use Storage;
use File;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Models\Address;

class RegisterController extends Controller
{
    //
    public function create()
    {
        //
        return view('register.create');
        // $categories = Category::get();
        // $subcategories = Subcategory::get();
        // return view('domain.create')->with('categories', $categories)->with('subcategories', $subcategories);

    }

    public function store(RegisterRequest $request)
    {
        $user = new User();
        $address = new Address();
        $deli_address = new Address();
        $user->firstname = $request['firstname'];
        $user->lastname = $request['lastname'];
        $user->nickname = $request['nickname'];
        $user->gender = $request['gender'];
        $user->age = $request['age'];
        $user->phone = $request['phone'];
        $user->email = $request['email'];
        $user->line = $request['lineid'];
        $user->facebook = $request['facebook'];
        $user->instragram = $request['instragram'];
        $user->role = 'user';
        $user->password = Hash::make($request['password']);
        // $user->password = Hash::make($data['password']),
        $user->save();

        if($request['profile_img']){
            $image = $request['profile_img'];
            $extension = $image->getClientOriginalExtension();
            $name = 'users/'.$user->id.'/'.'users-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $user->img = $name;
        }

        if($request['cardid_img']){
            $image = $request['cardid_img'];
            $extension = $image->getClientOriginalExtension();
            $name = 'users/'.$user->id.'/'.'cardid-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_private')->put($name, File::get($image));
            $user->idcard_img = $name;
        }

        if($request['qrcodeline_img']) {
            $image = $request['qrcodeline_img'];
            $extension = $image->getClientOriginalExtension();
            $name = 'users/'.$user->id.'/'.'lineimg-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $user->qrcode_line_img = $name;
        }

        $address->address = $request['address'];
        $address->subdistrict = $request['subdistrict'];
        $address->district = $request['district'];
        $address->province = $request['province'];
        $address->zipcode = $request['zipcode'];


        // return $user;
        $user->save();
        if($address->save()) {
            $address_id = $address->id;
           if($user->update(['address_id' => $address_id])) {
                //หลังจาก updateaddress ให้ไปอัพเดท delivery address
                $deli_address->address = $request['delivery_address'];
                $deli_address->subdistrict = $request['delivery_subdistrict'];
                $deli_address->district = $request['delivery_district'];
                $deli_address->province = $request['delivery_province'];
                $deli_address->zipcode = $request['delivery_zipcode'];

                if($deli_address->save()) {
                    $delivery_address_id = $deli_address->id;
                    $user->update(['deli_address_id' => $delivery_address_id]);
                }
           }
        }
        return response()->json($user, 200);
    }

}
