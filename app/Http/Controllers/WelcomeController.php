<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class WelcomeController extends Controller
{
    //
    public function index() {
        $products = Brand::get();
        return view('welcome')->with('products',$products);
    }

    public  function testhome()
    {
        $products = Brand::get();
        return view('testwelcome')->with('products',$products);
    }
}
