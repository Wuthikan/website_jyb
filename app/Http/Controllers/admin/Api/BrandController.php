<?php

namespace App\Http\Controllers\admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use File;
use Auth;

use App\Models\Brand;
use App\Models\LogBrand;
use App\Http\Resources\admin\BrandCollection;
use App\Http\Resources\admin\BrandResource;
use App\Http\Requests\admin\BrandRequest;

class BrandController extends Controller
{
    public function __construct()
    {
        $user = Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $brand = Brand::all();
        return new BrandCollection($brand);
    }

    public function trash()
    { 
        $brand = Brand::onlyTrashed()->get();
        return new BrandCollection($brand);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request)
    {
        $brand = new Brand();
        $brand->name = $request['name'];
        $brand->eng_name = $request['eng_name'];
        $brand->desc = $request['desc'];
        $brand->slug = str_slug($request['eng_name']);
        if($brand->slug == null){
            $brand->slug = 'msgyb';
        }
        $latestSlug = Brand::whereRaw("slug RLIKE '^{$brand->slug}(-[0-9]*)?$'")
                                ->latest('id')
                                ->value('slug');
        if($latestSlug) {
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $brand->slug .= '-' . ($number + 1);
        }

        $brand->save();

        if($request['image']){
            $image = $request['image'];
            $extension = $image->getClientOriginalExtension();
            $name = 'brands/'.$brand->id.'/'.'brand-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $brand->image = $name;
        }

        $brand->save();
        //Log
        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogBrand::create([
            'brand_id' => $brand->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'create',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);

        return response()->json($brand, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);
        return new BrandResource($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, $id)
    {
        $ipAddress = $request->getClientIp();  //Log
        $userId = Auth::user()->id;         //Log
        $oldValue = Brand::findOrFail($id)->toArray();  //Log

        $brand = Brand::findOrFail($id);
        $brand->name = $request->get('name');
        $brand->eng_name = $request->get('eng_name');
        $brand->slug = $request->get('slug');
        $brand->desc = $request->get('desc');

        if($request['image']){
            $file_old = $brand->image;
            Storage::disk('storage_global')->delete($file_old);

            $image = $request['image'];
            $extension = $image->getClientOriginalExtension();
            $name = 'brands/'.$brand->id.'/'.'brand-'. time() .'-'. $image->getClientOriginalName();
            Storage::disk('storage_global')->put($name, File::get($image));
            $brand->image = $name;
        }

        $brand->save();
        //Log
        $dirty = $brand->find($id)->toArray();

        $fields = [
            'name',
            'eng_name',
            'slug',
            'desc',
            'image'
        ];

        foreach ($fields as $name) {
            if ($dirty[$name] == $oldValue[$name]) {
                continue;
            }

            $old = (empty($oldValue[$name])) ? '' : $oldValue[$name];
            $new = (empty($dirty[$name])) ? '' : $dirty[$name];

            LogBrand::create([
                'brand_id' => $id,
                'editor_id' => $userId,
                'ip_appress' => $ipAddress,
                'date' => now(),
                'action' => 'edit',
                'field_name' => $name,
                'old_value' => $old,
                'new_value' => $new,
            ]);
        }

        return $brand;
    }

    public function restore(BrandRequest $request, $id){
        $brand = Brand::withTrashed()->find($id);
        $brand->restore();

        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogBrand::create([
            'brand_id' => $brand->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'restore',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);


        return response()->json($brand, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandRequest $request, $id)
    {
        $brand = Brand::withTrashed()->find($id);
        //Log
        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogBrand::create([
            'brand_id' => $brand->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'soft delete',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);

        $brand->delete();
        
        return response()->json($brand, 200);
    }

    public function forceDelete(BrandRequest $request, $id){
        $brand = Brand::withTrashed()->find($id);
        $image = $brand->image;
        if($image){
            Storage::disk('storage_global')->delete($image);
        }
        //Log
        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogBrand::create([
            'brand_id' => $brand->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'force delete',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);

        $brand->forceDelete();

        return response()->json($brand, 200);
    }

}
