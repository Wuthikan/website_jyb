<?php

namespace App\Http\Controllers\admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\PriceRate;
use App\Models\LogPriceRate;
use App\Http\Requests\admin\PriceRequest;

class PriceController extends Controller
{
    public function __construct()
    {
        $user = Auth::user();
    }

    public function index(Request $request)
    {   
        $paging = $request->input('page');
        $limit = $request->input('limit');
        $querysearch = $request->input('query');
        $sorting = $request->input('orderBy');
        $sortorder = $request->input('ascending');

        $price = PriceRate::
                select(
                    'id',
                    'set',
                    'desc',
                    'price',
                    'total_price',
                    'sort',
                    'level',
                    'brand_id',
                    'updated_at'
                );

        $count = $price->count();

        if ($querysearch) {
            $price->where('brand_id', '=', $querysearch);
        }else{
            return null;
        }

        if ($sorting) {
            $asc = $sortorder == 1 ? 'asc' : 'desc';
            $price->orderBy($sorting, $asc);
        } else {
            $price->orderBy('sort', 'asc');
        }

        // if ($paging == 1) {
        //     $price = $price->take($limit)->get();
        // } else {
        //     $price = $price->skip($limit * ($paging - 1))->take($limit)->get();
        // }  

        $price = $price->get();

        $data = [
            'data' => $price,
            'count' => $count
        ];

        return json_encode($data);
    }

    public function store(PriceRequest $request)
    {
        $price = new PriceRate();
        $price->set = $request['set'];
        $price->desc = $request['desc'];
        $price->price = $request['price'];
        $price->total_price = $request['total_price'];
        $price->level = $request['level'];
        $price->brand_id = $request['brand_id'];
        $price->sort = $request['sort'];

        $price->save();
        //Log
        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogPriceRate::create([
            'price_rate_id' => $price->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'create',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);

        return response()->json($price, 200);
    }
}
