<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;

class CardUserNavbarController extends Controller
{
    //
    public function index() {
        $carduser = Brand::all();
        return response()->json($carduser, 200);
    }
}
