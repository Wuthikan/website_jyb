<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;

class NavbarController extends Controller
{
    //
    public function index() {
        $products = Brand::all();
        return response()->json($products, 200);
    }
}
