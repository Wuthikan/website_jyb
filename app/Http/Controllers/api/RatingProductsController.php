<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;

class RatingProductsController extends Controller
{
    //
    public function index() {
        $brand = Brand::with('priceRates')->get();
        // $ratingProducts = PriceRate::all();
        return response()->json($brand, 200);
    }
}
