<?php

namespace App\Http\Controllers\api;
use Storage;
use File;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Member;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class isApiApproveUsersController extends Controller
{
    //
    public function index(Request $request) {
        // $paging = $request->input('page');
        // $limit = $request->input('limit');
        $user = User::
        join('member', 'users.id', '=', 'member.users_id')->
        join('brand', 'member.brand_id', '=', 'brand.id')->
        // where([
        //     ['member.status', '=', 'waiting'],
        //     ['member.adviser_id', '=', '00001'],
        // ])->
        select(
            'firstname',
            'lastname',
            'nickname',
            'age',
            'gender',
            'brand.name as brandname',
            'level',
            'member.created_at as dateofRegister',
        );

        $user = $user->get();
        $data = [
            'data' => $user,
        ];
        // dd($data);
        return json_encode($data);

    //    return $members = Member::with('user')->get();
    //    return Response::json(array('data' => $members));
    }

    //ดึงข้อมูลของคนที่มาสมัครเพื่อไปแสดงในตาราง approve user
    public function getDataIsRegisterUser($brand, $id) {
        // return $id;
        return $members = Member::with('user', 'brand', 'user.address', 'user.deliAddress', 'getMembers')->where([
            ['brand_id', '=' , $brand],
            ['adviser_id', '=' , $id],
            ['status', '=' , 'waiting']
        ])->orderBy('created_at', 'desc')->get();
    }

    //approve user
    public function updateApproveUser(Request $request) {
        $validated = $request->validate([
            'memberid' => 'required',
            'user_id' => 'required',
            'brand_id' => 'required',
            'level' => 'required',
            'slip' => 'required|mimes:jpeg,jpg,png,gif|max:5120',
        ]);

        $id = $request['memberid'];
        $user_id = $request['user_id'];
        $level = $request['level'];
        $brand_id = $request['brand_id'];

        $image = $request['slip'];
        $extension = $image->getClientOriginalExtension();
        $name = 'members/'.$id.'/'.'member-'. time() .'-'. $image->getClientOriginalName();
        Storage::disk('storage_global')->put($name, File::get($image));

        $member = Member::where('id', $id)
            ->update(['status' => 'approved',
                       'slip_img'=> $name ]);
    
        //Assigning Roles
        $user = User::find($user_id);
        $user->assignRole($brand_id.'-'.$level);
                        
        return response()->json($member, 200);
    }
    //delete approve user
    public function deleteUserApprove($id)
    {
        // return $id;
        $member = Member::where('id', $id)->forceDelete();
        return response()->json($member, 200);
    }

}
