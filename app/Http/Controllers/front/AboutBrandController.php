<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class AboutBrandController extends Controller
{
    //
    public function index() {
        $users = User::with('members')
                    ->whereHas('members', function ($query) {
                            $query->where('level', '=', 'dealer');
                    })->inRandomOrder()->limit(9)->get();
        return view('pages.about-brand.index')->with('users', $users);
    }
}
