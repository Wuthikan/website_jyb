<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Member;

class DetailMemberController extends Controller
{
    //
    public function show($id) {
        $members = Member::with('user', 'user.address')->find($id);
        // $mytopic = $topics->find(2);
        return view('pages.member.detail')->with('members', $members);
    }
}
