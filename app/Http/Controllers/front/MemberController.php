<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use App\Models\Member;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    //
    public function index() {
        // $members = Member::get();
        $members = Member::with('user')->where([['status', '=' , 'approved'],['level' ,'!=' , 'member']])->inRandomOrder()->get();
        return view('pages.member.index')->with('members',$members);
    }
    // Api
    public function getAdviser(Request $request) {

        $querysearch = $request->input('query');
        $brand_id = $request->input('brand');

        if($querysearch == null || $querysearch == ''){
            $advisors = Member::with('user.address')
                ->where('brand_id', '=', $brand_id)
                ->where('level', '!=', 'member')
                ->where('status', '!=', 'waiting')
                ->paginate(9);
        }else{
            $advisors = Member::with('user.address')
                ->where('brand_id', '=', $brand_id)
                ->where('level', '!=', 'member')
                ->whereHas('user', function ($query) use ($querysearch) {
                     $query->where('firstname', 'like', '%'.$querysearch.'%')
                            ->orWhere('lastname', 'like', '%' . $querysearch . '%');
                })
                ->orWhere('id', '=', $querysearch)
                ->where('status', '!=', 'waiting')
                ->paginate(9);
        }
        return response()->json($advisors, 200);
    }

}
