<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Member;

class ProductController extends Controller
{
    //
    public function index($slugname) {
        $brand = Brand::with('priceRates', 'brandImage')->where('slug', $slugname)->get();
        // $members = Member::with('user')->where('brand_id', $id)->get();
        return view('products.product')->with('brand',$brand);
        
        // return view('products.product')->with('brand',$brand)->with('members',$members);

    }
}
