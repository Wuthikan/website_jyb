<?php

namespace App\Http\Controllers\front\price;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PriceRate;

class PriceRateController extends Controller
{
    public function getPriceRate($brand) 
    {
        $price = PriceRate::where('brand_id', '=', $brand)
            ->orderBy('sort', 'ASC')
            ->get();

        return response()->json($price, 200);
    }
}
