<?php

namespace App\Http\Controllers\front\products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\Brand;
use App\Models\Cards;
use App\Models\Member;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    public function index($slugname, $idbrand) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }

        $authID = auth()->user()->id;
        // $authID = 12;
        $card = Cards::where('brand_id', $brand->id)->first();
        $member = Member::with('user', 'user.address', 'getAdvisor', 'getAdvisor.user')
                    ->where('brand_id', $brand->id)
                    ->whereHas('user', function ($query) use ($authID) {
                        $query->where('id', '=', $authID);
                        })
                    ->first();
    
    
        $team = null;
        $lineWork = null;
        if($member->getAdvisor){
            if($member->getAdvisor->level == 'ceo') {
                $team = 'สายตรงแบรนด์';
                $lineWork = 'สายตรงแบรนด์';
            }elseif($member->getAdvisor->level == 'dealer'){
                $team = $member->getAdvisor->user->nickname;
                $lineWork = 'สายตรงแบรนด์';
            }elseif($member->getAdvisor->level == 'team_leader' || $member->getAdvisor->level == 'member'){
                $team = $member->getAdvisor->user->nickname;
                $upUpLine = Member::with('user', 'getAdvisor', 'getAdvisor.user')
                                ->where('id', $member->getAdvisor->id)->first();
                if($upUpLine->getAdvisor == null || $upUpLine->getAdvisor == ''){
                    $lineWork = 'สายตรงแบรนด์';
                }elseif($upUpLine->getAdvisor->level == 'ceo'){
                    $lineWork = 'สายตรงแบรนด์';
                }else{
                    $lineWork = $upUpLine->getAdvisor->user->nickname;
                }
            }
        }
        $member['text_team'] = $team;
        $member['text_lineWork'] = $lineWork;

        return view('pages-user.dashboard.index')
                        ->with('brand',$brand)
                        ->with('card',$card)
                        ->with('member',$member);
    }
}
