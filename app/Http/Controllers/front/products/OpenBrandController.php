<?php

namespace App\Http\Controllers\front\products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\front\OpenBrandRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;

use App\Models\Brand;
use App\Models\Member;
use App\Models\LogMember;

class OpenBrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $brand = Brand::all();
        return view('register-products.index')->with('brands',$brand);
    }

    public function register($slugname, $idbrand) {
        //Check product
        $brand = Brand::with('priceRates', 'brandImage')->where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        //Check Permission
        if(auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/dashboard');
        }
        
        //Check status member
        $member = Member::with('user')
                    ->where('users_id', Auth::user()->id)
                    ->where('brand_id', $brand->id)
                    ->where('status', 'waiting')
                    ->first();
        if($member){
            return view('register-products.wait')->with('brand',$brand)->with('member',$member);
        }
   
        return view('register-products.create')->with('brand',$brand);
    }

    public function store(OpenBrandRequest $request)
    {
        $check = Member::where('users_id', Auth::user()->id)->where('brand_id',$request['brand_id'])->first();
        if($check){
            return false;
        }

        $member = new Member();  
        $member->brand_id = $request['brand_id'];
        $member->users_id = Auth::user()->id;
        $member->adviser_id = $request['adviser_id'];
        $member->level = $request['level'];
        $member->why_choose = $request['why_choose'];
        $member->know = $request['know'];
        $member->used = $request['used'];
        $member->rate_selected = $request['rate_selected'];
        $member->status = 'waiting';
        $member->save();

        //Log
        $ipAddress = $request->getClientIp();
        $userId = Auth::user()->id;
        LogMember::create([
            'member_id' => $member->id,
            'editor_id' => $userId,
            'ip_appress' => $ipAddress,
            'date' => now(),
            'action' => 'create',
            'field_name' => '',
            'old_value' => '',
            'new_value' => ''
        ]);

        return response()->json($member, 200);
    }
}
