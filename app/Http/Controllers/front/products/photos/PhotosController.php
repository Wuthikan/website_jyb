<?php

namespace App\Http\Controllers\front\products\photos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\Brand;
use App\Models\Photo;

class PhotosController extends Controller
{
    public function index($slugname, $idbrand) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }
        
        return view('pages-user.photos.index')->with('brand',$brand);
    }

    public function apiPhoto(Request $request, $idbrand) 
    {
        $photos = Photo::where('brand_id', '=', $idbrand)->paginate(12);

        return response()->json($photos, 200);
    }
}
