<?php

namespace App\Http\Controllers\front\products\team;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\Brand;
use App\Models\Member;

class TeamController extends Controller
{
    public function index($slugname, $idbrand) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }
        
        return view('pages-user.team.index')->with('brand',$brand);
    }

    public function indexWaiting($slugname, $idbrand) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }

        //get data user login and send to component
        $user_id = auth()->user()->id;
        //get user login เพื้่อส่งไปโชว์ตาราง approve โดยเพิ่มเงื่อนไขสลับแบรนด์ด้วย
        $member_login = Member::where([
            ['users_id', '=', $user_id],
            ['brand_id', '=', $brand->id]
            ])->first();

        return view('pages-user.team.wait')->with('brand',$brand)->with('member',$member_login);
    }

    public function showListUsers(Request $request, $idbrand, $userid)
    {
        $limit = $request->input('limit');
        $querysearch = $request->input('query');

        $memberBrand = Member::where('users_id', $userid)->first();

        if($querysearch == null || $querysearch == ''){
            $members = Member::with('user', 'brand', 'user.address', 'user.deliAddress', 'getMembers')->where([
                ['brand_id', '=' , $idbrand],
                ['adviser_id', '=' , $memberBrand->id],
                ['status', '!=' , 'waiting']
                ])->orderBy('created_at', 'asc')->paginate($limit);
        }else{
            $members = Member::with('user', 'brand', 'user.address', 'user.deliAddress', 'getMembers')->where([
                ['brand_id', '=' , $idbrand],
                ['adviser_id', '=' , $memberBrand->id],
                ['status', '!=' , 'waiting']
            ])
            ->whereHas('user', function ($query) use ($querysearch) {
                    $query->where('firstname', 'like', '%'.$querysearch.'%')
                        ->orWhere('lastname', 'like', '%' . $querysearch . '%')
                        ->orWhere('nickname', 'like', '%' . $querysearch . '%');
            })
            ->orWhere('id', 'like', '%' . $querysearch . '%')
            ->orderBy('created_at', 'asc')->paginate($limit);
        }
        

        return response()->json($members, 200);
    }
}
