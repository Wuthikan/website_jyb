<?php

namespace App\Http\Controllers\front\products\vdoes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\Brand;
use App\Models\Youtube;

class VdoesController extends Controller
{
    public function index($slugname, $idbrand) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }
        
        return view('pages-user.vdoes.index')->with('brand',$brand);
    }

    public function show($slugname, $idbrand, $id) 
    {
        //Check Brand
        $brand = Brand::where('id', $idbrand)->where('slug', $slugname)->first();
        if(!$brand){
            return redirect()->route('products');
        }
        // Check Permission
        if(!auth()->user()->hasPermissionTo($brand->id.'-normal')){
            return redirect($brand->slug.'/'.$brand->id.'/register');
        }
        
        $vdo = Youtube::where('id', $id)->first();

        return view('pages-user.vdoes.show')->with('brand',$brand)->with('vdo',$vdo);
    }

    public function apiVdos(Request $request, $idbrand) 
    {
        $vdoes = Youtube::where('brand_id', '=', $idbrand)->paginate(12);

        return response()->json($vdoes, 200);
    }

    public function apiVdosList($idbrand, $id) 
    {
        $vdoes = Youtube::where('brand_id', '=', $idbrand)
            ->where('id', '!=', $id)->inRandomOrder()->take(4)->get();

        return response()->json($vdoes, 200);
    }
}
