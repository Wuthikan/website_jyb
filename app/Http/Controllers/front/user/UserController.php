<?php

namespace App\Http\Controllers\front\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile() {
        return view('user.index');
    }

    public function editProfile() {
        return view('user.edit');
    }
    
    public function changePassword() {
        return view('user.change-password');
    }
}
