<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'firstname' => 'required',
            'lastname' => 'required',
            'nickname' => 'required',
            'gender' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'phone' => 'required',
            'lineid' => 'required',
            'address' => 'required',
            'subdistrict' => 'required',
            'district' => 'required',
            'province' => 'required',
            'zipcode' => 'required',
            'delivery_address' => 'required',
            'delivery_subdistrict' => 'required',
            'delivery_district' => 'required',
            'delivery_province' => 'required',
            'delivery_zipcode' => 'required',
            'profile_img'     => 'required|mimes:jpeg,jpg,png,gif|max:5120',
            'cardid_img'     => 'required|mimes:jpeg,jpg,png,gif|max:5120',
            'qrcodeline_img'     => 'mimes:jpeg,jpg,png,gif|max:5120',
        ];     
        // return $rules; 
    }

    // public function messages() {
    //     // return [
    //     //     'firstname.required' => 'กรุณากรอกชื่อ',
    //     //     'email.required' => 'กรุณากรอก อีเมลล์ ให้ถูกต้อง',
    //     //     'phone.required' => 'กรุณากรอกเบอร์โทรศัพท์',
    //     //     'line_id.required' => 'กรุณากรอก Line id',
    //     //     'address.required' => 'กรุณากรอกบ้านเลขที่',
    //     //     'subdistrict.required' => 'กรุณากรอกตำบล',
    //     //     'district.required' => 'กรุณากรอกอำเภอ',
    //     //     'province.required' => 'กรุณากรอกจังหวัด',
    //     //     'zipcode.required' => 'กรุณากรอกรหัสไปรษณีย์',
    //     //     'delivery_address.required' => 'กรุณากรอกบ้านเลขที่',
    //     //     'delivery_subdistrict.required' => 'กรุณากรอกอำเภอ',
    //     //     'delivery_province.required' => 'กรุณากรอกจังหวัด',
    //     //     'delivery_zipcode.required' => 'กรุณากรอกรหัสไปรษณีย์',
    //     // ];
    // }
    
}
