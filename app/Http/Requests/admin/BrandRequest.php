<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name'     => 'required|unique:brand,name',
                    'eng_name'     => 'required|unique:brand,eng_name',
                    'image' => 'mimes:jpeg,jpg,png,gif|max:5120'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name'     => 'required|unique:brand,name,'.$this->id,
                    'eng_name'     => 'required|unique:brand,eng_name,'.$this->id,
                    'slug'     => 'required|alpha_dash|unique:brand,slug,'.$this->id,
                    'image' => 'mimes:jpeg,jpg,png,gif|max:5120'
                ];
            default:break;
        }

    }

    public function messages()
    {
        return [
            'name.required'              => 'กรุณากรอกชื่อแบรนด์',
            'name.unique'              => 'ชื่อแบรนด์นี้มีอยู่ในระบบแล้ว',
            'eng_name.required'              => 'กรุณากรอกชื่อแบรนด์ภาษาอังกฤษ',
            'eng_name.unique'              => 'ชื่อแบรนด์นี้มีอยู่ในระบบแล้ว',
            'slug.required'              => 'กรุณากรอก slug',
            'slug.unique'              => 'slug นี้มีอยู่ในระบบแล้ว',
            'slug.alpha_dash'              => 'slug ต้องประกอบด้วยตัวอักษรตัวเลขขีดกลางและขีดล่างเท่านั้น ห้ามมีเว้นวรรค',
        ];
    }

}
