<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class PriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'set'     => 'required|integer',
                    'price'     => 'required|integer',
                    'total_price'     => 'required|integer',
                    'sort'     => 'required',
                    'level'     => 'required',
                    'brand_id'     => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'set'     => 'required|integer',
                    'price'     => 'required|integer',
                    'total_price'     => 'required|integer',
                    'sort'     => 'required',
                    'level'     => 'required',
                    'brand_id'     => 'required'
                ];
            default:break;
        }
    }

    public function messages()
    {
        return [
            'set.required'              => 'กรุณากรอกจำนวน',
            'set.integer'              => 'จำนวนต้องเป็นตัวเลขเท่านั้น',
            'price.required'              => 'กรุณากรอกราคา',
            'price.integer'              => 'ราคาต้องเป็นตัวเลขเท่านั้น',
            'total_price.required'              => 'กรุณากรอกรวมราคา',
            'total_price.integer'              => 'รวมราคาต้องเป็นตัวเลขเท่านั้น',
            'level.required'              => 'กรุณากรอกตำแหน่ง'
        ];
    }
}
