<?php

namespace App\Http\Requests\front;

use Illuminate\Foundation\Http\FormRequest;

class OpenBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'brand_id'     => 'required',
                    'adviser_id'     => 'required',
                    'level'     => 'required',
                    'why_choose'     => 'required',
                    'know'     => 'required',
                    'used'     => 'required',
                    'rate_selected'     => 'required',
                ];
            case 'PUT':
            case 'PATCH':
            default:break;
        }
    }
}
