<?php

namespace App\Http\Requests\front\profile;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'address_id' => 'required',
                    'deli_address_id' => 'required',
                    'add_address' => 'required',
                    'add_subdistrict' => 'required',
                    'add_district' => 'required',
                    'add_province' => 'required',
                    'add_zipcode' => 'required',
                    'deli_address' => 'required',
                    'deli_subdistrict' => 'required',
                    'deli_district' => 'required',
                    'deli_province' => 'required',
                    'deli_zipcode' => 'required'
                ];
            default:break;
        }
    }

    public function messages()
    {
        return [
            'add_address.required'      => 'กรุณากรอก ที่อยู่',
            'deli_address.required'     => 'กรุณากรอก ที่อยู่',
            'add_subdistrict.required'  => 'กรุณากรอก ตำบล / แขวง',
            'deli_subdistrict.required' => 'กรุณากรอก ตำบล / แขวง',
            'add_district.required'     => 'กรุณากรอก อำเภอ / เขต',
            'deli_district.required'    => 'กรุณากรอก อำเภอ / เขต',
            'add_province.required'     => 'กรุณากรอก จังหวัด',
            'deli_province.required'    => 'กรุณากรอก จังหวัด',
            'add_zipcode.required'      => 'กรุณากรอก รหัสไปรษณีย์',
            'deli_zipcode.required'     => 'กรุณากรอก รหัสไปรษณีย์'
        ];
    }
}
