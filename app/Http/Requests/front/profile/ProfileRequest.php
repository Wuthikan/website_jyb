<?php

namespace App\Http\Requests\front\profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'firstname' => 'required',
                    'lastname'  => 'required',
                    'nickname'  => 'required',
                    'email'     => 'required|email|unique:users,email,'.$this->id,
                    'gender'  => 'required',
                    'phone'  => 'required',
                    'img'     => 'mimes:jpeg,jpg,png,gif|max:5120'
                ];
            default:break;
        }
    }

    public function messages()
    {
        return [
            'firstname.required'              => 'กรุณากรอกชื่อ',
            'lastname.required'              => 'กรุณากรอกนามสกุล',
            'nickname.required'              => 'กรุณากรอกชื่อเล่น',
            'email.required'              => 'กรุณากรอก Email Address',
            'email.unique'              => 'Email นี้มีอยู่ในระบบแล้ว',
            'email.email'              => 'รูปแบบ Email ไม่ถูกต้อง',
            'gender.required'              => 'กรุณาเลือกเพศ',
            'phone.required'              => 'กรุณากรอกเบอร์โทรศัพท์',
        ];
    }
}
