<?php

namespace App\Http\Requests\front\profile;

use Illuminate\Foundation\Http\FormRequest;

class SocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [];
            case 'PUT':
            case 'PATCH':
                return [
                    'line' => 'required',
                    'qrcode_line_img'     => 'mimes:jpeg,jpg,png,gif|max:5120'
                ];
            default:break;
        }
    }

    public function messages()
    {
        return [
            'line.required'              => 'กรุณากรอก Line ID',
            'qrcode_line_img.mimes'              => 'jpeg, jpg, png, gif เท่านั้น',
            'qrcode_line_img.max'              => 'ไฟล์ขนาดไม่เกิน 10,000 MB',
        ];
    }
}
