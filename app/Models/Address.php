<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    public $primaryKey = 'id';

    protected $fillable = [
        'address',
        'subdistrict',
        'district',
        'province',
        'zipcode',
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $appends = ['full_address'];

    public function deliAddress()
    {
        return $this->hasOne(User::class, 'deli_address_id');
    }

    public function address()
    {
        return $this->hasOne(User::class, 'address_id');
    }

    public function getFullAddressAttribute()
    {
        return  $this->address. ' '.$this->subdistrict. ' '.$this->district
                . ' '.$this->province. ' '.$this->zipcode ;
    }
}
