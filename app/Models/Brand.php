<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    protected $table = 'brand';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'name',
        'eng_name',
        'slug',
        'desc',
        'image'
    ];

    protected $hidden = [
        'timestamp',
        'soft_delete',
    ];

    protected $appends = ['path_img'];

    public function members()
    {
        return $this->hasMany(Member::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function priceRates()
    {
        return $this->hasMany(PriceRate::class);
    }

    public function setups()
    {
        return $this->hasMany(Setup::class);
    }

    public function youtubes()
    {
        return $this->hasMany(Youtube::class);
    }

    public function getPathImgAttribute()
    {
        if($this->image){
            return   asset('image/global/'.$this->image ); 
        }else{
            return   asset('/image/public/default/0/defaul-image-1x1.png'); 
        }
    }

    public function brandImage()
    {
        return $this->hasMany(BrandImage::class);
    }

}
