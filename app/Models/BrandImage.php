<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandImage extends Model
{
    protected $table = 'brand_image';
    public $primaryKey = 'id';

    //
    protected $fillable = [
        'id',
        'brand_id',
        'img_path',
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $appends = ['path'];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function getPathAttribute()
    {
        return   asset('image/global/'.$this->img_path ); 
    }
}
