<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    protected $table = 'cards';
    public $primaryKey = 'id';

    protected $fillable = [
        'brand_id',
        'url',
        'name',
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'brand_id'
    ];

    protected $appends = ['path_img'];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function getPathImgAttribute()
    {
        if($this->url) {
            return   asset('image/global/'.$this->url );
        }else {
            return   asset('/image/public/default/0/defaul-image-1x1.png' );
        }
    }
}
