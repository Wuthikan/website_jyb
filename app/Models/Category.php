<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'category';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'name',
        'desc'
    ];

    protected $hidden = [
        'timestamp',
        'soft_delete'
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
