<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeleteMember extends Model
{

    protected $table = 'delete_member';
    public $primaryKey = 'id';

    protected $fillable = [
        'name',
        'brand_id',
        'member_id',
        'users_id',
        'adviser_id',
        'level',
        'why_choose',
        'know',
        'used',
        'rate_selected'
    ];

    protected $hidden = [
        'timestamp'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
}
