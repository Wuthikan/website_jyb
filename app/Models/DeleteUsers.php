<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeleteUsers extends Model
{
    protected $table = 'delete_users';
    public $primaryKey = 'id';

    protected $fillable = [
        'users_id',
        'role',
        'email',
        'password',
        'fristname',
        'lastname',
        'nickname',
        'phone',
        'line',
        'address',
        'subdistrict',
        'district',
        'province',
        'zipcode'
    ];

    protected $hidden = [
        'timestamp'
    ];
}
