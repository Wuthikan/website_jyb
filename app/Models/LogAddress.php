<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogAddress extends Model
{

    protected $table = 'log_address';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'address_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'address_id',
        'editor_id'
    ];

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
