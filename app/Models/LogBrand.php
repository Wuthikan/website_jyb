<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogBrand extends Model
{
    protected $table = 'log_brand';
    public $primaryKey = 'id';
    // protected $softDelete = true;

    protected $fillable = [
        'brand_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'brand_id',
        'editor_id'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
