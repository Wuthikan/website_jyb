<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogCards extends Model
{
    protected $table = 'log_cards';
    public $primaryKey = 'id';

    protected $fillable = [
        'cards_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'cards_id',
        'editor_id'
    ];

    public function photo()
    {
        return $this->belongsTo(Cards::class, 'cards_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
