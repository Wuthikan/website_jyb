<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogCategory extends Model
{
    protected $table = 'log_category';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'category_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'category_id',
        'editor_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
