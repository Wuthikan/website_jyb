<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogMember extends Model
{
    protected $table = 'log_member';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'member_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'member_id',
        'editor_id'
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
