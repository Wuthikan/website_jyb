<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogPhoto extends Model
{
    protected $table = 'log_photo';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'photo_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'photo_id',
        'editor_id'
    ];

    public function photo()
    {
        return $this->belongsTo(Photo::class, 'photo_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
