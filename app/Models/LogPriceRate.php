<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogPriceRate extends Model
{
    protected $table = 'log_price_rate';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'price_rate_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'price_rate_id',
        'editor_id'
    ];

    public function priceRate()
    {
        return $this->belongsTo(PriceRate::class, 'price_rate_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
