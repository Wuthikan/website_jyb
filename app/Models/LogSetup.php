<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogSetup extends Model
{
    protected $table = 'log_setup';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'setup_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'setup_id',
        'editor_id'
    ];

    public function setup()
    {
        return $this->belongsTo(Setup::class, 'setup_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
