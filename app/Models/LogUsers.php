<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUsers extends Model
{
    protected $table = 'log_users';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'users_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'users_id',
        'editor_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
