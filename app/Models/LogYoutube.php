<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogYoutube extends Model
{
    protected $table = 'log_youtube';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'youtube_id',
        'editor_id',
        'ip_appress',
        'date',
        'action',
        'field_name',
        'old_value',
        'new_value'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'youtube_id',
        'editor_id'
    ];

    public function youtube()
    {
        return $this->belongsTo(Youtube::class, 'youtube_id');
    }

    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }
}
