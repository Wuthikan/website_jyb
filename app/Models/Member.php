<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class Member extends Model
{
    use SoftDeletes;

    protected $table = 'member';
    public $primaryKey = 'id';
    public $incrementing = false;  //Gen ID
    protected $softDelete = true;

    protected $fillable = [
        'brand_id',
        'users_id',
        'adviser_id',
        'level',
        'why_choose',
        'know',
        'used',
        'rate_selected',
        'slip_img',
        'status',
    ];

    protected $hidden = [
        'timestamp',
        'soft_delete'
    ];

    protected $relations = [
        'brand_id',
        'users_id'
    ];
    
    //Gen ID
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->id= IdGenerator::generate(['table' => 'member', 'length' => 5, 'prefix' =>'0']);
        });
    }

    protected $appends = [
            'show_full_id',
            'text_level',
            'path_slip_img'
        ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function getAdvisor() //Up Line
    {
        return $this->belongsTo('App\Models\Member', 'adviser_id');
    }
    public function getMembers() // Down Line
    {
        return $this->hasMany('App\Models\Member', 'adviser_id');
    }

    public function getShowFullIdAttribute()
    {
        $full_id = 'MG-';
        if($this->level === 'member'){
            $full_id .= $this->getAdvisor['id'];
            $full_id .= '-';
            $full_id .= $this->id;
        }
        if($this->level === 'team_leader'){
            $full_id .= $this->getAdvisor['id'];
            $full_id .= '-';
            $full_id .= $this->id;
        }
        if($this->level === 'dealer' || $this->level === 'ceo' ){
            $full_id .= $this->id;
        }
        return $full_id;
    }

    public function getTextLevelAttribute()
    {
        $text = '';
        if($this->level === 'member'){
            $text = "ลูกทีม";
        }
        if($this->level === 'team_leader'){
            $text = "แม่ทีม";
        }
        if($this->level === 'dealer'){
            $text = "ดีลเลอร์";
        }
        if($this->level === 'ceo'){
            $text = "ซีอีโอ";
        }
        return $text;
    }

    public function getPathSlipImgAttribute()
    {
        if($this->slip_img){
            return   asset('image/global/'.$this->slip_img ); 
        }else{
            return   ''; 
        }
    }
}
