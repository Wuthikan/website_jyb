<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

    protected $table = 'photo';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'brand_id',
        'url',
        'name',
        'desc',
        'category_id',
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'brand_id',
        'category_id'
    ];

    protected $appends = ['path_img'];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function Category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getPathImgAttribute()
    {
        if($this->url) {
            return   asset('image/global/'.$this->url );
        }else {
            return   asset('/image/public/default/0/defaul-image-1x1.png' );
        }
    }

}
