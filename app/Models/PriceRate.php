<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceRate extends Model
{
    use SoftDeletes;

    protected $table = 'price_rate';
    public $primaryKey = 'id';
    protected $softDelete = true;

    protected $fillable = [
        'brand_id',
        'set',
        'type',
        'desc',
        'price',
        'total_price',
        'sort',
        'level'
    ];

    protected $hidden = [
        'timestamp',
        'soft_delete'
    ];

    protected $relations = [
        'brand_id'
    ];

    protected $appends = ['level_number'];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function getLevelNumberAttribute()
    {
        if($this->level == 'member'){
            return   1;
        }elseif($this->level == 'team_leader'){
            return   2;
        }elseif($this->level == 'dealer'){
            return   3;
        }elseif($this->level == 'ceo'){
            return   4;
        }
    }
}
