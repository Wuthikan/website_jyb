<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setup extends Model
{
    protected $table = 'setup';
    public $primaryKey = 'id';

    protected $fillable = [
        'brand_id',
        'slug',
        'page',
        'content',
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'brand_id'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
}