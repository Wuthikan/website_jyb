<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    protected $table = 'users';
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'email',
        'password',
        'firstname',
        'lastname',
        'nickname',
        'gender',
        'age',
        'phone',
        'idcard_img',
        'line',
        'facebook',
        'instragram',
        'qrcode_line_img',
        'deli_address_id',
        'address_id',
        'img',
        'banned',
        'last_login_at',
        'last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'timestamp',
        'soft_delete'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $relations = [
        'deli_address_id',
        'address_id'
    ];

    protected $appends = [
        'path_img',
        'path_qrcode_line_img',
        'path_idcard_img',
        'gender_text'
    ];

    public function members()
    {
        return $this->hasMany(Member::class, 'users_id');
    }

    public function deliAddress()
    {
        return $this->belongsTo(Address::class, 'deli_address_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function isAdmin(){
        if($this->role === 'super_admin' || $this->role === 'admin'){
            return true;
        }
        return false;
    }

    public function isSuperAdmin(){
        if($this->role === 'super_admin'){
            return true;
        }
        return false;
    }

    public function getPathImgAttribute()
    {
        if($this->img) {
            return   asset('image/global/'.$this->img );
        }else {
            return   asset('/image/public/users/default/user.png' );
        }
    }

    public function getPathQrcodeLineImgAttribute()
    {
        if($this->qrcode_line_img) {
            return   asset('image/global/'.$this->qrcode_line_img );
        }else {
            return   asset('/image/public/default/0/defaul-image-1x1.png' );
        }
    }

    public function getPathIdcardImgAttribute()
    {
        if($this->idcard_img) {
            return   asset('image/private/'.$this->idcard_img );
        }else {
            return   asset('/image/public/default/0/defaul-image-1x1.png' );
        }
    }

    public function getGenderTextAttribute()
    {
        if($this->gender == 'male') {
            return  'ชาย';
        }elseif($this->gender == 'female') {
            return   'หญิง';
        }
    }
}
