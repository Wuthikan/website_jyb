<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class Youtube extends Model
{
    protected $table = 'youtube';
    public $primaryKey = 'id';

    protected $fillable = [
        'brand_id',
        'url',
        'name',
        'desc'
    ];

    protected $hidden = [
        'timestamp'
    ];

    protected $relations = [
        'brand_id'
    ];

    protected $appends = ['path_img'];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function getPathImgAttribute()
    {
        if($this->url) {
            $url = $this->url;
            $queryString = parse_url($url, PHP_URL_QUERY);
            parse_str($queryString, $params);
            $v = $params['v'];  
            //DISPLAY THE IMAGE
            if(strlen($v)>0){
                return "http://img.youtube.com/vi/".$v."/0.jpg";
            }
        }else {
            return   asset('/image/public/default/0/defaul-image-1x1.png' );
        }
    }
}
