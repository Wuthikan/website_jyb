<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->enum('role', ['user', 'admin', 'super_admin'])
                  ->default('user')->comment('สิทธ์');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('firstname');
            $table->string('lastname')->nullable();
            $table->string('nickname')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('age')->nullable();
            $table->string('phone');
            $table->string('idcard_img')->nullable();
            $table->string('line')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instragram')->nullable();
            $table->string('qrcode_line_img')->nullable();
            $table->unsignedInteger('deli_address_id')->nullable();
            $table->unsignedInteger('address_id')->nullable();
            $table->string('img')->nullable();
            $table->string('banned')->enum('role', ['avilable', 'banned'])
                  ->default('avilable');  
            $table->datetime('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('deli_address_id')
             ->references('id')
             ->on('address')
             ->onDelete('set null');
            $table->foreign('address_id')
             ->references('id')
             ->on('address')
             ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
