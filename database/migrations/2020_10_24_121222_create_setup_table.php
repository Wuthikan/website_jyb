<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->unsignedInteger('brand_id');
            $table->string('slug')->unique();
            $table->string('page');
            $table->longText('content')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')
             ->references('id')
             ->on('brand')
             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup');
    }
}
