<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_rate', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->unsignedInteger('brand_id');
            $table->integer('set');
            $table->string('type')->nullable();
            $table->text('desc')->nullable();
            $table->decimal('price', 15, 0);
            $table->integer('total_price');
            $table->enum('level', ['member', 'team_leader', 'dealer'])
                  ->default('member')->comment('สิทธ์ Member');
            $table->integer('sort');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('brand_id')
             ->references('id')
             ->on('brand')
             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_rate');
    }
}
