<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->unsignedInteger('brand_id');
            $table->string('url');
            $table->string('name');
            $table->text('desc')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')
             ->references('id')
             ->on('brand')
             ->onDelete('cascade');

            $table->foreign('category_id')
             ->references('id')
             ->on('category')
             ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo');
    }
}
