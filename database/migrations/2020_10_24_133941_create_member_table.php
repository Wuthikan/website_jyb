<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->string('id')->primarykey();
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('users_id');
            $table->string('adviser_id')
                  ->comment('Up line');
            $table->enum('level', ['member', 'team_leader', 'dealer', 'ceo'])
                  ->default('member')
                  ->comment('สิทธ์ Member');
            $table->text('why_choose')->nullable();
            $table->text('know')->nullable();
            $table->enum('used', ['used', 'never'])->nullable();
            $table->string('rate_selected')->nullable();
            $table->string('slip_img')->nullable();
            $table->enum('status', ['approved', 'waiting'])
                  ->default('waiting');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('brand_id')
             ->references('id')
             ->on('brand')
             ->onDelete('cascade');
            $table->foreign('users_id')
             ->references('id')
             ->on('users')
             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
