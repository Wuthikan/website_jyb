<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeleteUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_users', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->integer('users_id');
            $table->enum('role', ['user', 'admin', 'super_admin'])
                  ->default('user')->comment('สิทธ์');
            $table->string('email');
            $table->string('password');
            $table->string('fristname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('nickname')->nullable();
            $table->string('phone')->nullable();
            $table->string('line')->nullable();
            $table->string('address')->nullable();
            $table->string('subdistrict')->nullable(); 
            $table->string('district')->nullable(); 
            $table->string('province')->nullable(); 
            $table->string('zipcode')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_users');
    }
}
