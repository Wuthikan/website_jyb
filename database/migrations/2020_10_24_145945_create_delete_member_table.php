<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeleteMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_member', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->integer('brand_id');
            $table->integer('member_id');
            $table->integer('users_id');
            $table->integer('adviser_id');
            $table->enum('level', ['member', 'team_leader', 'dealer', 'ceo'])
                  ->default('member')
                  ->comment('สิทธ์ Member');
            $table->text('why_choose')->nullable();
            $table->text('know')->nullable();
            $table->text('used')->nullable();
            $table->string('rate_selected')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_member');
    }
}
