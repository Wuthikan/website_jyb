<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_category', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->integer('category_id');
            $table->integer('editor_id');
            $table->string('ip_appress')->nullable();
            $table->timestamp('date')->nullable();
            $table->enum('action', [
                            'create',
                            'edit',
                            'soft delete',
                            'force delete',
                            'export excel',
                            'view',
                            'search',
                            'print',
                            'sent email',
                            'restore'
                        ])->nullable();
            $table->text('field_name')->nullable();
            $table->text('old_value')->nullable();
            $table->text('new_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_category');
    }
}
