<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->unsignedInteger('brand_id');
            $table->string('url');
            $table->string('name')->nullable();
            $table->string('color1')->nullable();
            $table->string('color2')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')
             ->references('id')
             ->on('brand')
             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
