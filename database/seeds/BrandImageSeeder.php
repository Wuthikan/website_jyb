<?php

use Illuminate\Database\Seeder;

use App\Models\BrandImage;

class BrandImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //avocado
        BrandImage::create([
            'brand_id' => 1,
            'img_path' => 'brands/1/1.webp',
        ]);
        BrandImage::create([
            'brand_id' => 1,
            'img_path' => 'brands/1/2.jpg',
        ]);
        BrandImage::create([
            'brand_id' => 1,
            'img_path' => 'brands/1/3.jpg',
        ]);

        //thai-herbal
        BrandImage::create([
            'brand_id' => 2,
            'img_path' => 'brands/2/1.webp',
        ]);
        BrandImage::create([
            'brand_id' => 2,
            'img_path' => 'brands/2/2.jpg',
        ]);
        BrandImage::create([
            'brand_id' => 2,
            'img_path' => 'brands/2/3.jpg',
        ]);

        //chamomile 3
        BrandImage::create([
            'brand_id' => 3,
            'img_path' => 'brands/3/1.webp',
        ]);
        BrandImage::create([
            'brand_id' => 3,
            'img_path' => 'brands/3/2.jpg',
        ]);
        BrandImage::create([
            'brand_id' => 3,
            'img_path' => 'brands/3/3.jpg',
        ]);

    }
}
