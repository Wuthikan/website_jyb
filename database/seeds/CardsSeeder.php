<?php

use Illuminate\Database\Seeder;

use App\Models\Cards;

class CardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cards::create([
            'brand_id' => 1,
            'url' => 'cards/1/avocado.webp',
            'name' => 'Avocado X Cacao',
            'color1' => '#fff',
            'color2' => '#343a40',
        ]);


        Cards::create([
            'brand_id' => 2,
            'url' => 'cards/2/thai-herbal.webp',
            'name' => 'Thai Hebal Body Cream',
            'color1' => '#fff',
            'color2' => '#644232',
        ]);

        Cards::create([
            'brand_id' => 3,
            'url' => 'cards/3/chamomile.webp',
            'name' => 'Chamomile',
            'color1' => '#633F27',
            'color2' => '#633F27',
        ]);
    }
}
