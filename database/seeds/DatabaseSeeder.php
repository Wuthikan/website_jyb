<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BrandSeeder::class);
        $this->call(PriceRateSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MemberSeeder::class);
        $this->call(BrandImageSeeder::class);
        $this->call(PhotoSeeder::class);
        $this->call(YoutubeSeeder::class);
        $this->call(CardsSeeder::class);
    }
}
