<?php

use Illuminate\Database\Seeder;

use App\Models\Member;

use Haruncpi\LaravelIdGenerator\IdGenerator;
use Faker\Factory;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        // กิฟ
        $member = Member::create([
            'brand_id' => 1,
            'users_id' => 1,
            'adviser_id' => '',
            'level' => 'ceo',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);

        $member = Member::create([
            'brand_id' => 2,
            'users_id' => 1,
            'adviser_id' => '',
            'level' => 'ceo',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);

        $member = Member::create([
            'brand_id' => 3,
            'users_id' => 1,
            'adviser_id' => '',
            'level' => 'ceo',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);
        // end กิฟ

        //Mook 
        Member::create([
            'brand_id' => 1,
            'users_id' => 4,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);

        Member::create([
            'brand_id' => 2,
            'users_id' => 4,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);

        Member::create([
            'brand_id' => 3,
            'users_id' => 4,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);

        //End Mook 

        //End Bom 
        Member::create([
            'brand_id' => 1,
            'users_id' => 5,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);
        Member::create([
            'brand_id' => 2,
            'users_id' => 5,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);
        Member::create([
            'brand_id' => 3,
            'users_id' => 5,
            'adviser_id' => '00001',
            'level' => 'dealer',
            'why_choose' => 'มีความสนใจการขายของออนไลน์ และแบรนด์ msgyb มี product ที่ใช้แล้วเห็นผลดี',
            'rate_selected' => '250',
            'status' => 'approved',
        ]);
        //End Boom

        //team_leader
        for ($i = 6; $i <= 30; $i++) {
            $member = new Member();  
            if($i<=15){
                $member->brand_id = 1;
            }elseif($i<=25){
                $member->brand_id = 2;
            }elseif($i<=30){
                $member->brand_id = 3;
            }
            $member->users_id = $i;
            $member->adviser_id = $faker->randomElement($array = array ('00001','00002','00003'));
            $member->level = $faker->randomElement($array = array ('team_leader'));
            $member->why_choose = $faker->text($maxNbChars = 200);
            $member->know = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $member->used = $faker->randomElement($array = array ('used','never'));
            $member->rate_selected = '100';
            $member->status = 'approved';
            $member->save();
        }
        //member
        for ($i = 31; $i <= 60; $i++) {
            $member = new Member();  
            if($i<=40){
                $member->brand_id = 1;
            }elseif($i<=50){
                $member->brand_id = 2;
            }elseif($i<=60){
                $member->brand_id = 3;
            }
            $member->users_id = $i;
            $member->adviser_id = $faker->randomElement($array = array ('00001','00002','00003'));
            $member->level = $faker->randomElement($array = array ('member'));
            $member->why_choose = $faker->text($maxNbChars = 200);
            $member->know = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $member->used = $faker->randomElement($array = array ('used','never'));
            $member->rate_selected = '100';
            $member->status = 'approved';
            $member->save();
        }
        //wait for arrpove
        for ($i = 61; $i <= 120; $i++) {
            $member = new Member();  
            $member->brand_id = $faker->randomElement($array = array (1,2,3));
            $member->users_id = $i;
            $member->adviser_id = $faker->randomElement($array = array ('00001','00002','00003'));
            $member->level = $faker->randomElement($array = array ('member','team_leader'));
            $member->why_choose = $faker->text($maxNbChars = 200);
            $member->know = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $member->used = $faker->randomElement($array = array ('used','never'));
            $member->rate_selected = '100';
            $member->status = 'waiting';
            $member->save();
        }
    }
}
