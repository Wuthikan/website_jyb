<?php

use Illuminate\Database\Seeder;

use App\Models\Photo;

class PhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++) {
            Photo::create([
                'brand_id' => 1,
                'url' => 'photo/1/2.jpg',
                'name' => 'Avocado X Cacao',
            ]);
            Photo::create([
                'brand_id' => 1,
                'url' => 'photo/2/3.jpg',
                'name' => 'Avocado X Cacao',
            ]);
        }
        for ($i = 20; $i <= 40; $i++) {
            Photo::create([
                'brand_id' => 2,
                'url' => 'photo/3/2.jpg',
                'name' => 'Thai Hebal Body Cream',
            ]);
            Photo::create([
                'brand_id' => 2,
                'url' => 'photo/4/3.jpg',
                'name' => 'Thai Hebal Body Cream',
            ]);
        }
        for ($i = 41; $i <= 80; $i++) {
            Photo::create([
                'brand_id' => 3,
                'url' => 'photo/5/2.jpg',
                'name' => 'น้ำตบคาโมมายล์',
            ]);
            Photo::create([
                'brand_id' => 3,
                'url' => 'photo/6/3.jpg',
                'name' => 'น้ำตบคาโมมายล์',
            ]);
        }
    }
}
