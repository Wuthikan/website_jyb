<?php

use Illuminate\Database\Seeder;

use App\Models\PriceRate;

class PriceRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++) {
            PriceRate::create([
                'brand_id' => $i,
                'set' => 10,
                'type' => 'เซท',
                'desc' => NULL,
                'price' => 195,
                'total_price' => 1950,
                'level' => 'member',
                'sort' => 1,
            ]);
    
            PriceRate::create([
                'brand_id' => 1,
                'set' => 25,
                'type' => 'เซท',
                'desc' => NULL,
                'price' => 190,
                'total_price' => 4750,
                'level' => 'member',
                'sort' => 2,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 50,
                'type' => 'เซท',
                'desc' => null,
                'price' => 185,
                'total_price' => 9250,
                'level' => 'member',
                'sort' => 3,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 100,
                'type' => 'เซท',
                'desc' => null,
                'price' => 180,
                'total_price' => 18000,
                'level' => 'member',
                'sort' => 4,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 250,
                'type' => 'เซท',
                'desc' => 'ทอง 1 กรัม',
                'price' => 175,
                'total_price' => 43750,
                'level' => 'team_leader',
                'sort' => 5,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 500,
                'type' => 'เซท',
                'desc' => 'ทอง 2 กรัม',
                'price' => 170,
                'total_price' => 85000,
                'level' => 'team_leader',
                'sort' => 6,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 1000,
                'type' => 'เซท',
                'desc' => 'ทอง 4 กรัม',
                'price' => 165,
                'total_price' => 165000,
                'level' => 'team_leader',
                'sort' => 6,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 3000,
                'type' => 'เซท',
                'desc' => 'ทอง 12 กรัม',
                'price' => 160,
                'total_price' => 480000,
                'level' => 'dealer',
                'sort' => 7,
            ]);
    
            PriceRate::create([
                'brand_id' => $i,
                'set' => 5000,
                'type' => 'เซท',
                'desc' => 'ทอง 20 กรัม',
                'price' => 155,
                'total_price' => 775500,
                'level' => 'dealer',
                'sort' => 8,
            ]);
        }
    
    }
}
