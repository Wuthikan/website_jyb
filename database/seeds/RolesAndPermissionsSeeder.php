<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions *admin
        Permission::create(['guard_name' => 'api', 'name' => 'crud']);
        Permission::create(['guard_name' => 'api', 'name' => 'hard delete']);
        Permission::create(['guard_name' => 'api', 'name' => 'crud admin']);
        // create roles and assign created permissions
        // this can be done as separate statements *admin
        $role_admin = Role::create(['guard_name' => 'api', 'name' => 'admin']);
        $role_admin->givePermissionTo('crud');
        $role_super_admin = Role::create(['guard_name' => 'api', 'name' => 'super-admin']);
        $role_super_admin->givePermissionTo(['crud', 'hard delete', 'crud admin']);

        // $user = User::find(1);
        // $user->assignRole($role_super_admin);
        // $user = User::find(2);
        // $user->assignRole($role_admin);

        $this->createBrandRole(1);
        $this->createBrandRole(2);
        $this->createBrandRole(3);

    }

    private function createBrandRole($name)
    {
        // create permissions *brand 
        Permission::create(['guard_name' => 'web', 'name' => $name.'-normal']);
        Permission::create(['guard_name' => 'web', 'name' => $name.'-manage_member']);
        // create roles and assign created permissions *brand 
        $role_member = Role::create(['guard_name' => 'web', 'name' => $name.'-member']);
        $role_member->givePermissionTo($name.'-normal');
        $role_team_leader = Role::create(['guard_name' => 'web', 'name' => $name.'-team_leader']);
        $role_team_leader->givePermissionTo([$name.'-normal', $name.'-manage_member']);
        $role_dealer = Role::create(['guard_name' => 'web', 'name' => $name.'-dealer']);
        $role_dealer->givePermissionTo([$name.'-normal', $name.'-manage_member']);
        $role_ceo = Role::create(['guard_name' => 'web', 'name' => $name.'-ceo']);
        $role_ceo->givePermissionTo([$name.'-normal', $name.'-manage_member']);

        // if($name==1){
        //     $user = User::find(1);
        //     $user->assignRole($role_ceo);
        //     $user = User::find(4);
        //     $user->assignRole($role_dealer);
        //     $user = User::find(5);
        //     $user->assignRole($role_team_leader);
        // }
    }
}
