<?php


use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Hash;
use Faker\Factory;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $user = User::create([
            'role' => 'super_admin',
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('admin@123456'),
            'firstname' => 'superadmin',
            'lastname' => 'admin',
            'nickname' => 'superAdmin',
            'phone' => $faker->numberBetween(1000, 9000),
            'line' => 'super_admin',
            'facebook' => $faker->lexify('F ??????????'),
            'gender' => 'female',
            'img' => 'users/1/1.jpg',
            'qrcode_line_img' => 'users/1/qrcodeline.jpg',
            'idcard_img' => 'users/1/idcard.jpg',
            'address_id' => $this->createAddress(),
            'deli_address_id' => $this->createAddress()
        ]);
        //assignRole
        $user->assignRole('1-ceo');
        $user->assignRole('2-ceo');
        $user->assignRole('3-ceo');
    
        $user = User::create([
            'role' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin@123456'),
            'firstname' => 'admin',
            'lastname' => ' ',
            'nickname' => 'Admin',
            'phone' => $faker->numberBetween(1000, 9000),
            'line' => 'admin',
            'gender' => 'male',
            'img' => 'users/2/2.jpg',
            'qrcode_line_img' => 'users/2/qrcodeline.jpg',
            'idcard_img' => 'users/2/idcard.jpg',
            'address_id' => $this->createAddress(),
            'deli_address_id' => $this->createAddress()
        ]);

        $user = User::create([
            'role' => 'user',
            'email' => 'test@test.com',
            'password' => Hash::make('test@123456'),
            'firstname' => 'TE',
            'lastname' => 'ST',
            'nickname' => 'Test',
            'phone' => $faker->numberBetween(1000, 9000),
            'line' => 'user',
            'gender' => 'male',
            'img' => 'users/3/3.jpg',
            'qrcode_line_img' => 'users/3/qrcodeline.jpg',
            'idcard_img' => 'users/3/idcard.jpg',
            'address_id' => $this->createAddress(),
            'deli_address_id' => $this->createAddress()
        ]);

        $user = User::create([
            'role' => 'user',
            'email' => 'mook@hotmail.com',
            'password' => Hash::make('mook@123456'),
            'firstname' => 'Phetcharat',
            'lastname' => 'Suksean',
            'nickname' => 'Mookies',
            'phone' => '095-2659465',
            'line' => 'mook@line',
            'facebook' => 'Phetcharat Suksean',
            'img' => 'users/4/4.jpg',
            'gender' => 'female',
            'qrcode_line_img' => 'users/4/qrcodeline.jpg',
            'idcard_img' => 'users/4/idcard.jpg',
            'address_id' => $this->createAddress(),
            'deli_address_id' => $this->createAddress()
        ]);
        //assignRole
        $user->assignRole('1-dealer');
        $user->assignRole('2-dealer');
        $user->assignRole('3-dealer');

        $user = User::create([
            'role' => 'user',
            'email' => 'forworkbomb001@gmail.com',
            'password' => Hash::make('mook@123456'),
            'firstname' => 'Panatjkorn',
            'lastname' => 'Phanudeja',
            'nickname' => 'Bombies',
            'phone' => '0956264859',
            'line' => 'Bomb@line',
            'img' => 'users/5/5.jpg',
            'gender' => 'male',
            'qrcode_line_img' => 'users/5/qrcodeline.jpg',
            'idcard_img' => 'users/5/idcard.jpg',
            'address_id' => $this->createAddress(),
            'deli_address_id' => $this->createAddress()
        ]);
        //assignRole
        $user->assignRole('1-dealer');
        $user->assignRole('2-dealer');
        $user->assignRole('3-dealer');

        //team_leader
        for ($i = 6; $i <= 30; $i++) {
            $user = new User();  
            $user->role = 'user';
            $user->email = $faker->email;
            $user->password = Hash::make('mook@123456');
            $user->firstname = $faker->firstname;
            $user->lastname = $faker->lastname;
            $user->nickname = $faker->userName;
            $user->phone = $faker->phoneNumber;
            $user->line = $faker->citySuffix;
            $user->facebook = $faker->lexify('F ??????????');
            $user->instragram = $faker->citySuffix;
            $user->gender = $faker->randomElement($array = array ('male','female'));
            $user->age = $faker->randomElement($array = array ('20','21','22','23','24','25',
                                                                '26','27','28','29','30','31',));
            if($i<18){
                $user->img = 'users/'.$i.'/'.$i.'.jpg';
                $user->qrcode_line_img = 'users/'.$i.'/qrcodeline.jpg';
                $user->idcard_img = 'users/'.$i.'/idcard.jpg';
            }
            $user->address_id = $this->createAddress();
            $user->deli_address_id = $this->createAddress();
            $user->save();

            //assignRole
            if($i<=15){
                $user->assignRole('1-team_leader');
            }elseif($i<=25){
                $user->assignRole('2-team_leader');
            }elseif($i<=30){
                $user->assignRole('3-team_leader');
            }
        }
        //member
        for ($i = 31; $i <= 60; $i++) {
            $user = new User();  
            $user->role = 'user';
            $user->email = $faker->email;
            $user->password = Hash::make('mook@123456');
            $user->firstname = $faker->firstname;
            $user->lastname = $faker->lastname;
            $user->nickname = $faker->userName;
            $user->phone = $faker->phoneNumber;
            $user->line = $faker->citySuffix;
            $user->facebook = $faker->lexify('F ??????????');
            $user->instragram = $faker->citySuffix;
            $user->gender = $faker->randomElement($array = array ('male','female'));
            $user->age = $faker->randomElement($array = array ('20','21','22','23','24','25',
                                                                '26','27','28','29','30','31',));
            $user->address_id = $this->createAddress();
            $user->deli_address_id = $this->createAddress();
            $user->save();

            //assignRole
            if($i<=40){
                $user->assignRole('1-member');
            }elseif($i<=50){
                $user->assignRole('2-member');
            }elseif($i<=60){
                $user->assignRole('3-member');
            }
        }

        //wait for arrpove
        for ($i = 61; $i <= 100; $i++) {
            $user = new User();  
            $user->role = 'user';
            $user->email = $faker->email;
            $user->password = Hash::make('mook@123456');
            $user->firstname = $faker->firstname;
            $user->lastname = $faker->lastname;
            $user->nickname = $faker->userName;
            $user->phone = $faker->phoneNumber;
            $user->line = $faker->citySuffix;
            $user->facebook = $faker->lexify('F ??????????');
            $user->instragram = $faker->citySuffix;
            $user->gender = $faker->randomElement($array = array ('male','female'));
            $user->age = $faker->randomElement($array = array ('20','21','22','23','24','25',
                                                                '26','27','28','29','30','31',));
            $randomImage = $faker->numberBetween($min = 6, $max = 17);
            $user->img = 'users/'.$randomImage.'/'.$randomImage.'.jpg';
            $user->qrcode_line_img = 'users/'.$i.'/qrcodeline.jpg';
            $user->idcard_img = 'users/'.$i.'/idcard.jpg';
            $user->address_id = $this->createAddress();
            $user->deli_address_id = $this->createAddress();
            $user->save();
        }
         //wait for arrpove No image
        for ($i = 101; $i <= 120; $i++) {
            $user = new User();  
            $user->role = 'user';
            $user->email = $faker->email;
            $user->password = Hash::make('mook@123456');
            $user->firstname = $faker->firstname;
            $user->lastname = $faker->lastname;
            $user->nickname = $faker->userName;
            $user->phone = $faker->phoneNumber;
            $user->line = $faker->citySuffix;
            $user->facebook = $faker->lexify('F ??????????');
            $user->instragram = $faker->citySuffix;
            $user->gender = $faker->randomElement($array = array ('male','female'));
            $user->age = $faker->randomElement($array = array ('20','21','22','23','24','25',
                                                                '26','27','28','29','30','31',));
            $user->address_id = $this->createAddress();
            $user->deli_address_id = $this->createAddress();
            $user->save();
        }
    }

    private function createAddress()
    {
        $faker = Factory::create();
        $res = Address::create([
            'address' => $faker->buildingNumber.' '.$faker->streetName,
            'subdistrict' => $faker->stateAbbr,
            'district' => $faker->state,
            'province' => $faker->city,
            'zipcode' => $faker->postcode,
            ]);
        return $res->id;
    }
}
