<?php

use Illuminate\Database\Seeder;

use App\Models\Youtube;

class YoutubeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 30; $i++) {
            Youtube::create([
                'brand_id' => 1,
                'url' => 'www.youtube.com/watch?v=R-CFh-0jyFs&feature=youtu.be&ab_channel=MurikukiZq',
                'name' => 'Avocado X Cacao',
            ]);
        }
        for ($i = 31; $i < 60; $i++) {
            Youtube::create([
                'brand_id' => 2,
                'url' => 'www.youtube.com/watch?v=R-CFh-0jyFs&feature=youtu.be&ab_channel=MurikukiZq',
                'name' => 'Thai Hebal Body Cream',
            ]);
        }
        for ($i = 61; $i < 90; $i++) {
            Youtube::create([
                'brand_id' => 3,
                'url' => 'www.youtube.com/watch?v=R-CFh-0jyFs&feature=youtu.be&ab_channel=MurikukiZq',
                'name' => 'น้ำตบคาโมมายล์',
            ]);
        }
    }
}
