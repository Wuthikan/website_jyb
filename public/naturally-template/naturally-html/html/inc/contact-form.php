<?php
	header('Content-Type: application/json; charset=UTF-8');
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;
	require 'phpmailer/src/Exception.php';
	require 'phpmailer/src/PHPMailer.php';
	require 'phpmailer/src/SMTP.php';

	/* 
		Configuration
		host: SMTP server
		username: SMTP username
		password: SMTP password
		SMTPSecure: PHPMailer::ENCRYPTION_SMTPS or PHPMailer::ENCRYPTION_STARTTLS
		port: TCP port to connect to
		fromEmail: From Email
		fromName: From Name
		subject: Subject
	*/
	$config = [
		'host' => 'smtp.yourhost.com',
		'username' => 'username@yourhost.com',
		'password' => 'yourpassword',
		'SMTPSecure' => PHPMailer::ENCRYPTION_SMTPS, 
		'port' => 465,
		'fromEmail' => 'from@yourhost.com',
		'fromName' => 'From Name',
		'toEmail' => 'to@yourhost.com',
		'toName' => 'To Name',
		'subject' => 'Contact Form',
	];
	$response = [
		'success' => false,
		'message' => '',
		'errors' => [],
	];

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$form_name = isset($_POST['name']) ? strip_tags($_POST['name']) : NULL;
		$form_email = isset($_POST['email']) ? strip_tags($_POST['email']) : NULL;
		$form_message = isset($_POST['message']) ? strip_tags($_POST['message']) : NULL;
		if (empty($form_name)) {
			$response['errors'][] = 'Name is required.';
		}
		if (empty($form_email)) {
			$response['errors'][] = 'Email is required.';
		}
		if (!filter_var($form_email, FILTER_VALIDATE_EMAIL)) {
			$response['errors'][] = 'Incorrect email address.';
		}
		if (empty($form_message)) {
			$response['errors'][] = 'Message is required.';
		}
		if (!empty($response['errors'])) {
			$response['success'] = false;
			$response['message'] = 'Message could not be sent.';
			echo json_encode($response);
		} else {
			$mail = new PHPMailer(true);
			$form_body = "<strong>Name: </strong>". $form_name ."<br>";
			$form_body .= "<strong>Email: </strong>". $form_email ."<br>";
			$form_body .= "<strong>Message: </strong>". nl2br($form_message) ."<br>";
			try {
				$mail->isSMTP();
				$mail->Host = $config['host'];
				$mail->SMTPAuth = true;
				$mail->Username = $config['username'];
				$mail->Password = $config['password'];
				$mail->SMTPSecure = $config['SMTPSecure'];
				$mail->Port = $config['port'];
				$mail->setFrom($config['fromEmail'], $config['fromName']);
				$mail->addAddress($config['toEmail'], $config['toName']);
				$mail->isHTML(true);
				$mail->Subject = $config['subject'];
				$mail->Body = $form_body;
				$mail->send();
				$response['success'] = true;
				$response['message'] = 'Message has been sent.';
				echo json_encode($response);
			} catch (Exception $e) {
				$response['success'] = false;
				$response['message'] = 'Message could not be sent. Error: ' . $mail->ErrorInfo;
				echo json_encode($response);
			}
		}
	} else {
		$response['success'] = false;
		$response['message'] = 'Message could not be sent. Error: Bad request method.';
		echo json_encode($response);
	}
?>