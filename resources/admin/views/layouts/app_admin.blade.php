<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Admin Msgyb') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

    @yield('head')
</head>
<body>
    <div id="app">
        <layout-page>
            @yield('content')
        </layout-page>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/admin/admin.js') }}" defer></script>
    @yield('script')
</body>
</html>
