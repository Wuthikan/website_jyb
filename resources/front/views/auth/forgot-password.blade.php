@extends('layouts.app')
@section('content')
    <div class="section section-forgot-password">
		<div class="container">
			<div class="grid">
				<div class="grid_row">
					<div class="grid_col is-12 is-lg-6">
						<form>
							<div class="grid">
								<div class="grid_row is-30 is-md-40">
									<div class="grid_col is-12">
										<h3 class="text-secondary">Forgot Password ?</h3>
										<p>Please enter your email address. You will receive a link to create a new password via email.</p>
									</div>
									<div class="grid_col is-12">
										<label class="form_label">Email address <span class="form_label_require">*</span></label>
										<input class="form_control_input" type="text" name="email" placeholder="Enter your email ..." required>
									</div>
									<div class="grid_col is-12">
										<div class="grid">
											<div class="grid_row is-30 is-md-40">
												<div class="grid_col is-12 is-sm-6">
													<button class="button is-block is-grey" type="submit">
														<span class="button_text">Reset Password</span>
														<span class="button_icon icon is-next"></span>
													</button>
												</div>
												<div class="grid_col is-12 is-sm-6">
													<a class="button is-block is-grey" href="login-1.html">
														<span class="button_text">Login</span>
														<span class="button_icon icon is-next"></span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection