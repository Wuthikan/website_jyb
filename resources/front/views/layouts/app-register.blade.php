<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.script.header-user')

    @yield('head')
</head>
<body >
    <div id="app-user">
        <!-- Loading -->
        <div class="loading is-active" data-ntr-loading>
            <span class="loading_loader"></span>
        </div>
        <!-- Back Top -->
        <div class="back-top" data-ntr-backtop>
            <span class="back-top_icon icon is-arrow-up"></span>
        </div>
        <div class="bg-green-image">
        @include('layouts.components.navbar-register')
            @yield('content')
        @include('layouts.components.footer')
        </div>
    </div>
   
    @include('layouts.script.footer-user')
    @yield('script')
</body>
</html>
