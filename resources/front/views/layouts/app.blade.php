<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.script.header')

    @yield('head')
</head>
<body>
    <div id="app">
        @include('layouts.components.navbar')
        <main class="py-4 pb-0 pt-sm-0">
            @include('layouts.components.login')
            <!-- Loading -->
                <div class="loading is-active" data-ntr-loading>
                    <span class="loading_loader"></span>
                </div>
            {{-- back to top --}}
            <div class="back-top" data-ntr-backtop>
                <span class="back-top_icon icon is-arrow-up"></span>
            </div>
            @yield('content')
        </main>
        @include('layouts.components.footer')
    </div>

    @include('layouts.script.footer')
    @yield('script')
</body>
</html>
