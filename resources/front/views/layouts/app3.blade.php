<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Msgyb Thailand') }}</title>

    {{-- template --}}
    <link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.magnific-popup.css') }}" rel="stylesheet">
    <!-- Slick Carousel CSS -->
    <link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/aos.css') }}" rel="stylesheet">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('naturally-template/naturally-html/html/assets/css/style.css') }}">
    {{-- end-template --}}
    @yield('head')
</head>

<body>

	<!-- Header -->
	<div class="header is-overlay" data-ntr-header>
		<div class="header_holder"></div>
		<div class="header_container">
			<div class="header_search">
				<div class="container">
					<form class="header_search_form">
						<input class="header_search_input" type="text" name="keyword" placeholder="Search ..." required>
						<button class="header_search_button" type="submit"><span class="icon is-search"></span></button>
						<button class="header_search_close" type="button"><span class="icon is-close"></span></button>
					</form>
				</div>
			</div>
			<div class="container">
				<div class="header_top">
					<div class="header_phone">
						<span class="header_phone_icon icon is-support"></span>
						<span class="header_phone_text">+1-800-123-45-67</span>
					</div>
					<div class="header_menu">
						<div class="header_menu_item">
							<a class="header_menu_link header_search_open" href="#">
								<span class="header_menu_link_icon icon is-search"></span>
								<span class="header_menu_link_text">Search</span>
							</a>
						</div>
						<div class="header_menu_item">
							<a class="header_menu_link" href="login-1.html">
								<span class="header_menu_link_icon icon is-avatar"></span>
								<span class="header_menu_link_text">Account</span>
							</a>
						</div>
					</div>
					<div class="header_lang">
						<div class="header_lang_label">
							<span class="header_lang_label_icon icon is-internet"></span>
							<span class="header_lang_label_text">English</span>
						</div>
						<div class="header_lang_list">
							<ul class="header_lang_items">
								<li class="header_lang_item">
									<a class="header_lang_link" href="#">
										<img class="header_lang_link_flag" src="assets/img/flags/gb.svg" alt="English">
										<span class="header_lang_link_text">English</span>
									</a>
								</li>
								<li class="header_lang_item">
									<a class="header_lang_link" href="#">
										<img class="header_lang_link_flag" src="assets/img/flags/fr.svg" alt="French">
										<span class="header_lang_link_text">French</span>
									</a>
								</li>
								<li class="header_lang_item">
									<a class="header_lang_link" href="#">
										<img class="header_lang_link_flag" src="assets/img/flags/de.svg" alt="German">
										<span class="header_lang_link_text">German</span>
									</a>
								</li>
								<li class="header_lang_item">
									<a class="header_lang_link" href="#">
										<img class="header_lang_link_flag" src="assets/img/flags/es.svg" alt="Spanish">
										<span class="header_lang_link_text">Spanish</span>
									</a>
								</li>
								<li class="header_lang_item">
									<a class="header_lang_link" href="#">
										<img class="header_lang_link_flag" src="assets/img/flags/ru.svg" alt="Russian">
										<span class="header_lang_link_text">Russian</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="header_bottom">
					<a class="header_logo" href="home-1.html">
						<img src="assets/img/logo.png" alt="Naturally">
					</a>
					<nav class="header_nav">
						<a class="header_nav_close" href="#">
							<span class="header_nav_close_text">Close</span>
							<span class="header_nav_close_icon icon is-close"></span>
						</a>
						<ul>
							<li class="has-sub">
								<a href="#">Home</a>
								<span class="header_nav_arrow">
									<span class="icon is-arrow-down2"></span>
								</span>
								<div class="header_nav_sub">
									<ul>
										<li><a href="home-1.html">Homepage 1</a></li>
										<li><a href="home-2.html">Homepage 2</a></li>
										<li><a href="home-3.html">Homepage 3</a></li>
									</ul>
								</div>
							</li>
							<li class="has-sub">
								<a href="#">Pages</a>
								<span class="header_nav_arrow">
									<span class="icon is-arrow-down2"></span>
								</span>
								<div class="header_nav_sub">
									<ul>
										<li class="has-sub">
											<a href="#">About</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="about-1.html">About 1</a></li>
													<li><a href="about-2.html">About 2</a></li>
												</ul>
											</div>
										</li>
										<li><a href="team-1.html">Team</a></li>
										<li><a href="gallery-1.html">Gallery</a></li>
										<li class="has-sub">
											<a href="#">Contact</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="contact-1.html">Contact 1</a></li>
													<li><a href="contact-2.html">Contact 2</a></li>
												</ul>
											</div>
										</li>
										<li class="has-sub">
											<a href="#">Account</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="login-1.html">Login</a></li>
													<li><a href="register-1.html">Register</a></li>
													<li><a href="forgot-password-1.html">Forgot Password</a></li>
												</ul>
											</div>
										</li>
										<li><a href="404-1.html">404 Page</a></li>
									</ul>
								</div>
							</li>
							<li class="has-sub">
								<a href="#">Shop</a>
								<span class="header_nav_arrow">
									<span class="icon is-arrow-down2"></span>
								</span>
								<div class="header_nav_sub">
									<ul>
										<li class="has-sub">
											<a href="#">Listing</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="shop-1.html">Listing 1</a></li>
													<li><a href="shop-2.html">Listing 2</a></li>
													<li><a href="shop-3.html">Listing 3</a></li>
													<li><a href="shop-4.html">Listing 4</a></li>
												</ul>
											</div>
										</li>
										<li><a href="shop-single-1.html">Single</a></li>
										<li><a href="shop-cart-1.html">Cart</a></li>
										<li><a href="shop-checkout-1.html">Checkout</a></li>
									</ul>
								</div>
							</li>
							<li class="has-sub">
								<a href="#">Blog</a>
								<span class="header_nav_arrow">
									<span class="icon is-arrow-down2"></span>
								</span>
								<div class="header_nav_sub">
									<ul>
										<li class="has-sub">
											<a href="#">Listing</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="blog-1.html">Listing 1</a></li>
													<li><a href="blog-2.html">Listing 2</a></li>
													<li><a href="blog-3.html">Listing 3</a></li>
												</ul>
											</div>
										</li>
										<li class="has-sub">
											<a href="#">Single</a>
											<span class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</span>
											<div class="header_nav_sub">
												<ul>
													<li><a href="blog-single-1.html">Single 1</a></li>
													<li><a href="blog-single-2.html">Single 2</a></li>
													<li><a href="blog-single-3.html">Single 3</a></li>
												</ul>
											</div>
										</li>
										<li class="has-sub">
											<a href="#">Post Types</a>
											<div class="header_nav_arrow">
												<span class="icon is-arrow-down2"></span>
											</div>
											<div class="header_nav_sub">
												<ul>
													<li><a href="blog-single-1.html">Image</a></li>
													<li><a href="blog-single-1-gallery.html">Gallery</a></li>
													<li><a href="blog-single-1-video.html">Video</a></li>
													<li><a href="blog-single-1-audio.html">Audio</a></li>
													<li><a href="blog-single-1-quote.html">Quote</a></li>
													<li><a href="blog-single-1-link.html">Link</a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="has-mega">
								<a href="#">Elements</a>
								<span class="header_nav_arrow">
									<span class="icon is-arrow-down2"></span>
								</span>
								<div class="header_nav_mega">
									<div class="grid">
										<div class="grid_row is-xl-30">
											<div class="grid_col is-12 is-xl-col">
												<ul class="header_nav_mega_menu">
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#accordion">Accordion</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#alert">Alert</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#badge">Badge</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#breadcrumb">Breadcrumb</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#button">Button</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#counter">Counter</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#form">Form</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#google-map">Google Map</a></li>
												</ul>
											</div>
											<div class="grid_col is-12 is-xl-col">
												<ul class="header_nav_mega_menu">
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#grid">Grid</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#icon">Icon</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#label">Label</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#lightbox-gallery">Lightbox Gallery</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#masonry-layout">Masonry Layout</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#modal">Modal</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#pagination">Pagination</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#range-slider">Range Slider</a></li>
												</ul>
											</div>
											<div class="grid_col is-12 is-xl-col">
												<ul class="header_nav_mega_menu">
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#social-icon">Social Icon</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#tab">Tab</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#tooltip">Tooltip</a></li>
													<li class="header_nav_mega_menu_item"><a class="header_nav_mega_menu_link" href="features-elements.html#typography">Typography</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</nav>
					<div class="header_nav_toggle">
						<span class="icon is-menu"></span>
					</div>
					<div class="header_cart">
						<div class="header_cart_label">
							<span class="header_cart_label_icon icon is-bag"></span>
							<span class="header_cart_label_text">$31.70 / 3 Item</span>
						</div>
						<div class="header_cart_detail">
							<div class="header_cart_products">
								<table>
									<tbody>
										<tr>
											<td class="is-photo">
												<a href="shop-single-1.html">
													<img src="assets/img/blank.gif" srcset="assets/img/product-13-100x100.jpg 1x, assets/img/product-13-200x200.jpg 2x" alt="Product Name">
												</a>
											</td>
											<td class="is-qty">1x</td>
											<td class="is-name"><a href="shop-single-1.html">Organic D'Anjou Lemon</a></td>
											<td class="is-price">$6.40</td>
											<td class="is-delete"><a href="#"><span class="icon is-close"></span></a></td>
										</tr>
										<tr>
											<td class="is-photo">
												<a href="shop-single-1.html">
													<img src="assets/img/blank.gif" srcset="assets/img/product-12-100x100.jpg 1x, assets/img/product-12-200x200.jpg 2x" alt="Product Name">
												</a>
											</td>
											<td class="is-qty">1x</td>
											<td class="is-name"><a href="shop-single-1.html">Organic D'Anjou Tomato</a></td>
											<td class="is-price">$7.20</td>
											<td class="is-delete"><a href="#"><span class="icon is-close"></span></a></td>
										</tr>
										<tr>
											<td class="is-photo">
												<a href="shop-single-1.html">
													<img src="assets/img/blank.gif" srcset="assets/img/product-11-100x100.jpg 1x, assets/img/product-11-200x200.jpg 2x" alt="Product Name">
												</a>
											</td>
											<td class="is-qty">1x</td>
											<td class="is-name"><a href="shop-single-1.html">Organic D'Anjou Lettuce</a></td>
											<td class="is-price">$8.10</td>
											<td class="is-delete"><a href="#"><span class="icon is-close"></span></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="header_cart_footer">
								<table>
									<tbody>
										<tr>
											<td class="is-subtotal">Subtotal</td>
											<td class="is-subtotal-value">$21.70</td>
										</tr>
										<tr>
											<td class="is-shipping">Shipping</td>
											<td class="is-shipping-value">$10.00</td>
										</tr>
										<tr>
											<td class="is-tax">Tax</td>
											<td class="is-tax-value">$0.00</td>
										</tr>
										<tr>
											<td class="is-total">Total</td>
											<td class="is-total-value">$31.70</td>
										</tr>
										<tr>
											<td class="is-view">
												<a class="button is-block is-small is-grey" href="shop-cart-1.html">
													<span class="button_text">View Cart</span>
												</a>
											</td>
											<td class="is-checkout">
												<a class="button is-block is-small is-grey" href="shop-checkout-1.html">
													<span class="button_text">Checkout</span>
												</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Header End -->
	<!-- Section Hero -->
	<div class="section section-hero">
		<div class="container">
			<div class="section-hero_inner">
				<div class="section-hero_heading" data-aos="ntr-fade-up" data-aos-once="true">
					<h1 class="section-hero_heading_title">Healthy foods</h1>
					<p class="section-hero_heading_text">Your journey to a healthier life starts here.</p>
					<p>
						<a class="link is-color-secondary" href="shop-1.html">
							<span class="link_text">Shop Now</span>
							<span class="link_icon icon is-next"></span>
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Section Hero End -->
	<!-- Section Categories -->
	<div class="section section-custom-1">
		<div class="container">
			<!-- Slider Categories -->
			<div class="slider-categories" data-ntr-slider-categories>
				<div class="slick-slider">
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-13-400x400.jpg 1x, assets/img/category-13-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Lemon</span>
							<span class="slider-categories_item_count">96 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-12-400x400.jpg 1x, assets/img/category-12-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Tomato</span>
							<span class="slider-categories_item_count">91 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-11-400x400.jpg 1x, assets/img/category-11-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Lettuce</span>
							<span class="slider-categories_item_count">86 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-10-400x400.jpg 1x, assets/img/category-10-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Patato</span>
							<span class="slider-categories_item_count">81 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-9-400x400.jpg 1x, assets/img/category-9-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Mushroom</span>
							<span class="slider-categories_item_count">76 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-8-400x400.jpg 1x, assets/img/category-8-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Banana</span>
							<span class="slider-categories_item_count">71 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-7-400x400.jpg 1x, assets/img/category-7-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Apple</span>
							<span class="slider-categories_item_count">66 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-6-400x400.jpg 1x, assets/img/category-6-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Bean</span>
							<span class="slider-categories_item_count">61 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-5-400x400.jpg 1x, assets/img/category-5-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Carrot</span>
							<span class="slider-categories_item_count">56 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-4-400x400.jpg 1x, assets/img/category-4-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Corn</span>
							<span class="slider-categories_item_count">51 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-3-400x400.jpg 1x, assets/img/category-3-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Grape</span>
							<span class="slider-categories_item_count">46 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-2-400x400.jpg 1x, assets/img/category-2-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Cherry</span>
							<span class="slider-categories_item_count">41 Products</span>
						</a>
					</div>
					<div class="slick-slide">
						<a class="slider-categories_item" href="shop-1.html">
							<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/category-1-400x400.jpg 1x, assets/img/category-1-800x800.jpg 2x" alt="Category Name"></span>
							<span class="slider-categories_item_name">Brocolli</span>
							<span class="slider-categories_item_count">36 Products</span>
						</a>
					</div>
				</div>
			</div>
			<!-- Slider Categories End -->
		</div>
	</div>
	<!-- Section Categories End -->
	<!-- Section Featured Products -->
	<div class="section">
		<div class="container">
			<!-- Heading -->
			<div class="heading" data-aos="ntr-fade-up" data-aos-once="true">
				<h2 class="heading_title">Featured Products</h2>
				<p class="heading_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<!-- Heading End -->
			<!-- Products -->
			<div class="grid">
				<div class="grid_row is-40">
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-13-400x400.jpg 1x, assets/img/product-13-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Lemon</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-12-400x400.jpg 1x, assets/img/product-12-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Tomato</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="400">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-11-400x400.jpg 1x, assets/img/product-11-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Lettuce</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="600">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-10-400x400.jpg 1x, assets/img/product-10-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Patato</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-9-400x400.jpg 1x, assets/img/product-9-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Mushroom</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-8-400x400.jpg 1x, assets/img/product-8-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Banana</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="400">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-7-400x400.jpg 1x, assets/img/product-7-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Apple</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
					<div class="grid_col is-12 is-sm-6 is-md-4 is-xl-3" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="600">
						<!-- Product -->
						<div class="shop-product hover-shadow">
							<div class="shop-product_photo">
								<a href="shop-single-1.html">
									<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-6-400x400.jpg 1x, assets/img/product-6-800x800.jpg 2x" alt="Product Name"></span>
								</a>
							</div>
							<div class="shop-product_content">
								<div class="shop-product_body">
									<div class="shop-product_title">
										<a href="shop-single-1.html">Organic Bean</a>
									</div>
									<div class="shop-product_rating">
										<span class="rating">
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="icon is-star-outline"></span>
											<span class="rating_rate" style="width:80%;">
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
												<span class="icon is-star"></span>
											</span>
										</span>
									</div>
								</div>
								<div class="shop-product_footer">
									<div class="shop-product_prices">
										<span class="shop-product_prices_old">$14.40</span>
										<span class="shop-product_prices_current">$12.40</span>
									</div>
									<div class="shop-product_actions">
										<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
										<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
										<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
									</div>
								</div>
							</div>
						</div>
						<!-- Product End -->
					</div>
				</div>
			</div>
			<!-- Products End -->
		</div>
	</div>
	<!-- Section Featured Products End -->
	<!-- Section Deals -->
	<div class="section section-custom-2">
		<div class="container">
			<!-- Slider Deals -->
			<div class="slider-deals" data-ntr-slider-deals>
				<div class="slider-deals_heading" data-aos="ntr-fade-up" data-aos-once="true">
					<h2 class="slider-deals_title">Hot deals of the week</h2>
					<p class="slider-deals_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin elit et lectus scelerisque aliquet. Aenean vitaemi. Mauris at augue vel tortor suscipit cursus.</p>
					<p>
						<a class="link is-color-secondary" href="shop-1.html">
							<span class="link_text">Shop Now</span>
							<span class="link_icon icon is-next"></span>
						</a>
					</p>
				</div>
				<div class="slider-deals_products">
					<div class="slick-slider">
						<div class="slick-slide">
							<!-- Product -->
							<div class="shop-product">
								<div class="shop-product_photo">
									<a href="shop-single-1.html">
										<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-8-400x400.jpg 1x, assets/img/product-8-800x800.jpg 2x" alt="Product Name"></span>
									</a>
								</div>
								<div class="shop-product_content">
									<div class="shop-product_body">
										<div class="shop-product_title">
											<a href="shop-single-1.html">Organic D'Anjou Banana</a>
										</div>
										<div class="shop-product_rating">
											<span class="rating">
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="rating_rate" style="width:80%;">
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
												</span>
											</span>
										</div>
									</div>
									<div class="shop-product_footer">
										<div class="shop-product_prices">
											<span class="shop-product_prices_old">$14.40</span>
											<span class="shop-product_prices_current">$12.40</span>
										</div>
										<div class="shop-product_actions">
											<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
											<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
										</div>
									</div>
								</div>
							</div>
							<!-- Product End -->
						</div>
						<div class="slick-slide">
							<!-- Product -->
							<div class="shop-product">
								<div class="shop-product_photo">
									<a href="shop-single-1.html">
										<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-7-400x400.jpg 1x, assets/img/product-7-800x800.jpg 2x" alt="Product Name"></span>
									</a>
								</div>
								<div class="shop-product_content">
									<div class="shop-product_body">
										<div class="shop-product_title">
											<a href="shop-single-1.html">Organic D'Anjou Apple</a>
										</div>
										<div class="shop-product_rating">
											<span class="rating">
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="rating_rate" style="width:80%;">
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
												</span>
											</span>
										</div>
									</div>
									<div class="shop-product_footer">
										<div class="shop-product_prices">
											<span class="shop-product_prices_old">$14.40</span>
											<span class="shop-product_prices_current">$12.40</span>
										</div>
										<div class="shop-product_actions">
											<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
											<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
										</div>
									</div>
								</div>
							</div>
							<!-- Product End -->
						</div>
						<div class="slick-slide">
							<!-- Product -->
							<div class="shop-product">
								<div class="shop-product_photo">
									<a href="shop-single-1.html">
										<span class="aspect-ratio is-1x1"><img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/product-3-400x400.jpg 1x, assets/img/product-3-800x800.jpg 2x" alt="Product Name"></span>
									</a>
								</div>
								<div class="shop-product_content">
									<div class="shop-product_body">
										<div class="shop-product_title">
											<a href="shop-single-1.html">Organic D'Anjou Grape</a>
										</div>
										<div class="shop-product_rating">
											<span class="rating">
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="icon is-star-outline"></span>
												<span class="rating_rate" style="width:80%;">
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
													<span class="icon is-star"></span>
												</span>
											</span>
										</div>
									</div>
									<div class="shop-product_footer">
										<div class="shop-product_prices">
											<span class="shop-product_prices_old">$14.40</span>
											<span class="shop-product_prices_current">$12.40</span>
										</div>
										<div class="shop-product_actions">
											<a class="shop-product_actions_item" href="#" title="Add to Cart"><span class="icon is-cart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Quick View"><span class="icon is-view"></span></a>
											<a class="shop-product_actions_item" href="#" title="Add to Wishlist"><span class="icon is-heart"></span></a>
											<a class="shop-product_actions_item" href="#" title="Compare"><span class="icon is-exchange"></span></a>
										</div>
									</div>
								</div>
							</div>
							<!-- Product End -->
						</div>
					</div>
				</div>
			</div>
			<!-- Slider Deals End -->
		</div>
	</div>
	<!-- Section Deals End -->
	<!-- Section Features -->
	<div class="section">
		<div class="container">
			<!-- Heading -->
			<div class="heading" data-aos="ntr-fade-up" data-aos-once="true">
				<h2 class="heading_title">Why Choose Us</h2>
				<p class="heading_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<!-- Heading End -->
			<!-- Features -->
			<div class="grid">
				<div class="grid_row is-60 is-xl-120">
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-apple"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">Organic</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-watch"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">Fast & Easy</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-quality"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">High Quality</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-box"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">Free Shipping</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-support"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">Customer Support</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
					<div class="grid_col is-12 is-lg-6" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
						<!-- Feature -->
						<div class="feature">
							<div class="feature_icon">
								<div class="icon is-gift"></div>
							</div>
							<div class="feature_info">
								<h3 class="feature_title">Made with Love</h3>
								<p class="feature_summary">Lorem ipsum dolor sit amet, adipiscing elit. Cras nec ligula id ex malesuada consectetur volutpat.</p>
							</div>
						</div>
						<!-- Feature End -->
					</div>
				</div>
			</div>
			<!-- Features End -->
		</div>
	</div>
	<!-- Section Features End -->
	<!-- Section Testimonials -->
	<div class="section section-custom-3">
		<div class="container">
			<div class="grid">
				<div class="grid_row">
					<div class="grid_col is-xl-7" data-aos="ntr-fade-up" data-aos-once="true">
						<!-- Slider Testimonials -->
						<div class="slider-testimonials" data-ntr-slider-testimonials>
							<h2 class="slider-testimonials_title">Testimonials</h2>
							<div class="slick-slider">
								<div class="slick-slide">
									<div class="slider-testimonials_item">
										<div class="slider-testimonials_item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel purus fringilla, lobortis libero ut, interdum lacus. Ut quis urna sollicitudin, iaculis dolor sed, sodales mi. Proin a velit convallis, fermentum orci in, rutrum diam. Duis elementum odio a pharetra commodo. Sed eget massa sit amet nunc egestas tristique.</div>
										<div class="slider-testimonials_item_user">
											<div class="slider-testimonials_item_user_name">Tiana Mcdonnell</div>
											<div class="slider-testimonials_item_user_title">Sales Manager</div>
										</div>
									</div>
								</div>
								<div class="slick-slide">
									<div class="slider-testimonials_item">
										<div class="slider-testimonials_item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel purus fringilla, lobortis libero ut, interdum lacus. Ut quis urna sollicitudin, iaculis dolor sed, sodales mi. Proin a velit convallis, fermentum orci in, rutrum diam. Duis elementum odio a pharetra commodo. Sed eget massa sit amet nunc egestas tristique.</div>
										<div class="slider-testimonials_item_user">
											<div class="slider-testimonials_item_user_name">Terry Figueroa</div>
											<div class="slider-testimonials_item_user_title">Marketing Manager</div>
										</div>
									</div>
								</div>
								<div class="slick-slide">
									<div class="slider-testimonials_item">
										<div class="slider-testimonials_item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel purus fringilla, lobortis libero ut, interdum lacus. Ut quis urna sollicitudin, iaculis dolor sed, sodales mi. Proin a velit convallis, fermentum orci in, rutrum diam. Duis elementum odio a pharetra commodo. Sed eget massa sit amet nunc egestas tristique.</div>
										<div class="slider-testimonials_item_user">
											<div class="slider-testimonials_item_user_name">Kaycee Hess</div>
											<div class="slider-testimonials_item_user_title">Human Resources</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Slider Testimonials End -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Section Testimonials End -->
	<!-- Section Blog -->
	<div class="section">
		<div class="container">
			<!-- Heading -->
			<div class="heading" data-aos="ntr-fade-up" data-aos-once="true">
				<h2 class="heading_title">Organic Blog</h2>
				<p class="heading_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<!-- Heading End -->
			<!-- Slider Blog -->
			<div class="slider-blog" data-ntr-slider-blog>
				<div class="slick-slider">
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-9-400x300.jpg 1x, assets/img/blog-post-9-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Keep the Green Out of the Potato</a></h3>
							<p class="blog-post_meta">April 20, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-8-400x300.jpg 1x, assets/img/blog-post-8-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Healthiest Beans and Legumes</a></h3>
							<p class="blog-post_meta">April 19, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-7-400x300.jpg 1x, assets/img/blog-post-7-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Simple and Delicious Keto Salads</a></h3>
							<p class="blog-post_meta">April 18, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-6-400x300.jpg 1x, assets/img/blog-post-6-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Friendly Breakfast Ideas</a></h3>
							<p class="blog-post_meta">April 17, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-5-400x300.jpg 1x, assets/img/blog-post-5-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Drinking Water in the Morning</a></h3>
							<p class="blog-post_meta">April 16, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-4-400x300.jpg 1x, assets/img/blog-post-4-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Healthiest Vegetables on Earth</a></h3>
							<p class="blog-post_meta">April 15, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-3-400x300.jpg 1x, assets/img/blog-post-3-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">A Healthier Cooking Oil</a></h3>
							<p class="blog-post_meta">April 14, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-2-400x300.jpg 1x, assets/img/blog-post-2-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">Work-Friendly Lunch Recipes</a></h3>
							<p class="blog-post_meta">April 13, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
					<div class="slick-slide">
						<!-- Post -->
						<div class="blog-post">
							<a class="blog-post_photo" href="blog-single-1.html">
								<span class="aspect-ratio is-4x3">
									<img class="aspect-ratio_object lazyload" src="assets/img/blank.gif" data-srcset="assets/img/blog-post-1-400x300.jpg 1x, assets/img/blog-post-1-800x600.jpg 2x" alt="Post Title">
								</span>
							</a>
							<h3 class="blog-post_title"><a href="blog-single-1.html">This Doctor Is Also a Farmer</a></h3>
							<p class="blog-post_meta">April 12, 2019 By <a href="#">Bobbie Ryan</a></p>
							<p class="blog-post_summary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
							<p class="blog-post_readmore">
								<a class="link is-color-secondary" href="blog-single-1.html">
									<span class="link_text">Read more</span>
									<span class="link_icon icon is-next"></span>
								</a>
							</p>
						</div>
						<!-- Post End -->
					</div>
				</div>
			</div>
			<!-- Slider Blog End -->
		</div>
	</div>
	<!-- Section Blog End -->
	<!-- Footer -->
	<div class="footer">
		<div class="container">
			<div class="footer_top">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-6 is-xl-4">
							<div class="footer_about">
								<div class="footer_about_logo">
									<img src="assets/img/logo.png" alt="Naturally">
								</div>
								<div class="footer_about_text">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
								</div>
							</div>
							<div class="footer_contact">
								<div class="footer_contact_item">
									<h3>Address</h3>
									<p>241 Devonshire Street Lebanon, PA 17042</p>
								</div>
								<div class="footer_contact_item">
									<h3>Phone</h3>
									<p>+1-800-123-45-67</p>
								</div>
								<div class="footer_contact_item">
									<h3>Email</h3>
									<p><a href="mailto:sales@naturally.com">sales@naturally.com</a></p>
								</div>
							</div>
						</div>
						<div class="grid_col is-12 is-lg-6 is-xl-4 is-offset-xl-4">
							<div class="footer_newsletter">
								<h3>Sign Up &amp; Save</h3>
								<p>Get exclusive offers, free shipping events & more by signing up for our promotional emails.</p>
								<form class="footer_newsletter_form">
									<input class="footer_newsletter_form_input" type="email" name="email" placeholder="Enter your e-mail address ..." required>
									<button class="footer_newsletter_form_button" type="submit"><span class="icon is-send"></span></button>
								</form>
							</div>
							<div class="footer_social">
								<h3>Follow us</h3>
								<div class="social-icons">
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-facebook"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-twitter"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-instagram"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-youtube"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer_bottom">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-col">
							<div class="footer_copyright">Copyright © 2019 Naturally.com. All rights reserved.</div>
						</div>
						<div class="grid_col is-12 is-lg-auto">
							<nav class="footer_menu">
								<ul class="footer_menu_items">
									<li class="footer_menu_item"><a class="footer_menu_link" href="#">Privacy Policy</a></li>
									<li class="footer_menu_item"><a class="footer_menu_link" href="#">Terms of Use</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer End -->
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.min.js') }}" defer></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.magnific-popup.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.waypoints.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.countTo.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/slick/slick.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/imagesloaded.pkgd.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/isotope.pkgd.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/lazysizes.min.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/aos.js') }}" ></script>
    <script src="{{ asset('naturally-template/naturally-html/html/assets/js/scripts.js') }}" ></script>
    {{-- <script src="{{ asset('naturally-template/naturally-html/html/assets/js/custom.js') }}" ></script> --}}

</body>

