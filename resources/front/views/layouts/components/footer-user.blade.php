<section class="footer">
    <div class="footer">
		<div class="container">
			<div class="footer_top">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-6 is-xl-4">
							<div class="footer_contact">
								<div class="footer_contact_item">
									<h3>Address</h3>
									<p>103/1 บ้านสุพรรณ ซอยสุขสมบูรณ์ ตำบลชมภู อำเภอสารภี จังหวัดเชียงใหม่ 50140</p>
								</div>
								<div class="footer_contact_item">
									<h3>Phone</h3>
									<p>099-143-4442</p>
								</div>
								<div class="footer_contact_item">
									<h3>Email</h3>
									<p>msgybthailand@gmail.com</p>
								</div>
							</div>
						</div>
						<div class="grid_col is-12 is-lg-6 is-xl-4 is-offset-xl-4">
							<div class="footer_newsletter">
								<h3>Sign Up &amp; Save</h3>
								<form class="footer_newsletter_form">
									<input class="footer_newsletter_form_input" type="email" name="email" placeholder="Enter your e-mail address ..." required>
									<button class="footer_newsletter_form_button" type="submit"><span class="icon is-send"></span></button>
								</form>
							</div>
							<div class="footer_social">
								<h3>Follow us</h3>
								<div class="social-icons">
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-facebook"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-twitter"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-instagram"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-youtube"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer_bottom">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-col">
							<div class="footer_copyright">Copyright © 2020 msgybthailand.com. All rights reserved. </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


