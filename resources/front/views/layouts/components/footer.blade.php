<section class="footer pb-0 pt-0">
    <div class="footer footer-home pb-60" style="background: #575943 !important;">
		<div class="container">
			<div class="footer_top">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-6 is-xl-4">
							{{-- <div class="footer_about">
								<div class="footer_about_logo">
									<img src="assets/img/logo.png" alt="Naturally">
								</div>
								<div class="footer_about_text">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec nisi ac turpis laoreet auctor sed sed turpis.</p>
								</div>
							</div> --}}
							<div class="footer_contact mt-md-0">
								<div class="footer_contact_item">
									<p class="footer-text-topic">Address</p>
									<p class="text-white footer-text">
                                        เลขที่ 103/1 หมู่ที่ 3 บ้านสุพรรณ
                                        ซอยสุขสมบูรณ์ ตำบลชมภู
                                        อำเภอสารภี จังหวัดเชียงใหม่
                                        50140
                                    </p>
								</div>
								<div class="footer_contact_item">
									<p class="footer-text-topic">Phone</p>
									<p class="text-white footer-text">099-1434442</p>
								</div>
								<div class="footer_contact_item">
									<p class="footer-text-topic">Email</p>
									<p class="text-white"><a href="mailto:msgybthailand@gmail.com" class="text-white footer-text">msgybthailand@gmail.com</a></p>
								</div>
							</div>
						</div>
						<div class="grid_col is-12 is-lg-6 is-xl-4 is-offset-xl-4">
                            <div>
                                <img class="lazyload rounded-circle" src="{{ asset('img/webp/logo.webp') }}"
                                style="width: 150px; height:150px; background-color:white; padding:0.5em;" alt="msgyblogo">
                            </div>
							<div class="footer_social">
								<p class="text-white footer-text">MSGYB BRAND (หจก. จีวายบี เทรดดิ้ง)</p>
								<div class="social-icons">
                                    <a href="https://facebook.com/msgybthailand" class="pr-2" target="_blank"><img src="/img/facebook3.webp" alt="" class="social-icons_link lazyload detail-member-icon"></a>
                                    <a href="https://www.instagram.com/ceo.msgyb/" class="pr-2" target="_blank"><img src="/img/instragram.png" alt="" class="social-icons_link lazyload detail-member-icon"></a>
                                    <a href="https://lin.ee/nQ39i9B" target="_blank"><img src="/img/line.webp" alt="" class="social-icons_link lazyload detail-member-icon"></a>
									{{-- <a class="social-icons_link" href="https://m.facebook.com/msgybthailand" target="_blank"><span class="icon is-facebook"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-twitter"></span></a>
									<a class="social-icons_link" href="https://www.instagram.com/ceo.msgyb/" target="_blank"><span class="icon is-instagram"></span></a>
									<a class="social-icons_link" href="#" target="_blank"><span class="icon is-youtube"></span></a> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer_bottom">
				<div class="grid">
					<div class="grid_row is-40 is-lg-60">
						<div class="grid_col is-12 is-lg-col">
							<div class="footer_copyright text-white footer-text">Copyright © 2020 msgybthailand.com. All rights reserved.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


