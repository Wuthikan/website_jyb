<!-- Login Modal -->
<div class="modal fade" id="loginModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header border-none ">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-box">
            <h1 class="modal-title border-none w-100 text-center font-weight-bold mb-3" id="staticBackdropLabel">
                <li><span class="title-darkgreen">LOGIN TO</span> <span class="title-green">MSGYB</span></li>
            </h1>
            <div class="d-flex justify-content-center mb-3">
                <div class="col-md-4"  style="border: 1px solid#7E866F;"></div>
            </div>
            <div class="container description-login text-center mb-3">
                <span>Enter your information to <br> access your account</span>
            </div>
            <login-agent></login-agent>
        </div>
        {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div> --}}
    </div>
    </div>
</div>