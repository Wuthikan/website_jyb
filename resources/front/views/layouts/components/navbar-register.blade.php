{{-- หลังlogin แล้ว --}}
<div class="header is-overlay" id="navbar-user" data-ntr-header>
    <div class="header_holder"></div>
    <div class="header_container">
        <div class="container">
            <div class="header_top">
                <div class="header_phone">
                    <a href="/products">
                        <img src="{{ asset('img/webp/logo.webp') }}" class="lazyload logo-brand" alt="Naturally">
                    </a>
                </div>
                <div class="header_menu">
                    <div class="header_menu_item">
                        <div class="header_menu_link top-text-nav-user" >
                            <span class="header_menu_link_icon icon is-avatar"></span>
                            <span class="header_menu_link_text">{{ Auth::user()->nickname}}</span>
                        </div>
                        <div class="header_menu_link_list">
                            <ul class="header_menu_link_items">
                                <li class="header_menu_link_item">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/profile">
                                        <span class="ti-user"></span>
                                        <span class="header_menu_sub_link_text">ข้อมูลส่วนตัว</span>
                                    </a>
                                </li>
                                <li class="header_menu_link_item">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/profile/change-password">
                                        <span class="ti-unlock"></span>
                                        <span class="header_menu_sub_link_text">เปลี่ยนรหัสผ่าน</span>
                                    </a>
                                </li>
                                <li class="header_menu_link_item">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/logout">
                                        <span class="ti-shift-right"></span>
                                        <span class="header_menu_sub_link_text">ออกจากระบบ</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header_lang">
                    <div class="header_lang_label">
                        <span class="header_lang_label_icon icon is-box top-text-nav-user"></span>
                        <span class="header_lang_label_text top-text-nav-user">Products</span>
                    </div>
                    <top-droupdown-product></top-droupdown-product>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section section-top-blackgroud">
    <div class="container">
        <div class="section-top-blackgroud_inner">
            
        </div>
    </div>
</div>