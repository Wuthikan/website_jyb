{{-- หลังlogin แล้ว --}}
<div class="header is-overlay" id="navbar-user" data-ntr-header>
    <div class="header_holder"></div>
    <div class="header_container">
        <div class="container">
            <div class="header_top">
                <div class="header_phone">
                    <a href="/products">
                        <img src="{{ asset('img/webp/logo.webp') }}" class="lazyload logo-brand" alt="Naturally">
                    </a>
                </div>
                <div class="header_menu">
                    <div class="header_menu_item">
                        <div class="header_menu_link top-text-nav-user" >
                            <span class="header_menu_link_icon icon is-avatar"></span>
                            <span class="header_menu_link_text">{{ Auth::user()->nickname}}</span>
                        </div>
                        <div class="header_menu_link_list">
                            <ul class="header_menu_link_items">
                                <li class="header_menu_link_item">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/profile">
                                        <span class="ti-user"></span>
                                        <span class="header_menu_sub_link_text">ข้อมูลส่วนตัว</span>
                                    </a>
                                </li>
                                <li class="header_menu_link_item ">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/profile/change-password">
                                        <span class="ti-unlock"></span>
                                        <span class="header_menu_sub_link_text">เปลี่ยนรหัสผ่าน</span>
                                    </a>
                                </li>
                                <li class="header_menu_link_item">
                                    <a class="header_menu_sub_link top-text-nav-user-sub" href="/logout">
                                        <span class="ti-shift-right"></span>
                                        <span class="header_menu_sub_link_text">ออกจากระบบ</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header_lang" >
                    <div class="header_lang_label ">
                        <span class="header_lang_label_icon icon is-box top-text-nav-user"></span>
                        <span class="header_lang_label_text top-text-nav-user">Products</span>
                    </div>
                    <top-droupdown-product></top-droupdown-product>
                </div>
            </div>
            <div class="header_bottom">
                <a class="header_logo text-logo" href="#">
                    {{ $brand->eng_name }}
                </a>
                <nav class="header_nav">
                    <a class="header_nav_close text-nav-user" href="#">
                        <span class="header_nav_close_text">Close</span>
                        <span class="header_nav_close_icon icon is-close"></span>
                    </a>
                    <ul>
                        <li class="ml-xl-4">
                            <a class="text-nav-user" href="/{{$brand->slug}}/{{$brand->id}}/dashboard">Dashboard</a>
                        </li>
                        <li class="ml-xl-4 has-sub">
                            <a class="text-nav-user" href="#">Teams</a>
                            <span class="header_nav_arrow">
                                <span class="icon is-arrow-down2"></span>
                            </span>
                            <div class="header_nav_sub">
                                <ul>
                                    <li><a class="text-nav-user-sub" href="/{{$brand->slug}}/{{$brand->id}}/team">ทั้งหมด</a></li>
                                    <li><a class="text-nav-user-sub" href="/{{$brand->slug}}/{{$brand->id}}/team/waiting">รอดำเนิการ</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="ml-xl-4">
                            <a class="text-nav-user" href="/{{$brand->slug}}/{{$brand->id}}/photos">Image Gallery</a>
                        </li>
                        <li class="ml-xl-4">
                            <a class="text-nav-user" href="/{{$brand->slug}}/{{$brand->id}}/videos">Videos</a>
                        </li>
                    </ul>
                </nav>
                <div class="header_nav_toggle icon-navbar-mobile">
                    <span class="icon is-menu"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section section-top-blackgroud">
    <div class="container">
        <div class="section-top-blackgroud_inner">
            
        </div>
    </div>
</div>