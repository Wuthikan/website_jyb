
<div class="header is-overlay" data-ntr-header>
    <div class="header_holder"></div>
    <div class="header_container">
        <div class="container">
            <div class="header_bottom">
                <a class="header_logo" href="/">
                    <img src="{{ asset('img/webp/logo.webp') }}" class="lazyload logo-brand" alt="Naturally">
                </a>
                <nav class="header_nav">
                    <a class="header_nav_close" href="#">
                        <span class="header_nav_close_text">Close</span>
                        <span class="header_nav_close_icon icon is-close"></span>
                    </a>
                    <ul>
                        <li class="ml-xl-5">
                            <a class="text-nav" href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="has-sub ml-xl-5">
                            <a class="sub-text-nav" href="#">Pages</a>
                            <span class="header_nav_arrow">
                                <span class="icon is-arrow-down2"></span>
                            </span>
                            <div class="header_nav_sub">
                                <ul>
                                    <li class="has-sub">
                                        <a href="{{route('about-brand')}}">About Brand</a>
                                        <span class="header_nav_arrow">
                                            <span class="icon is-arrow-down2"></span>
                                        </span>
                                    </li>
                                    <li><a href="{{route('member')}}">Team</a></li>
                                    <li class="has-sub">
                                        {{-- <a href="{{route('contact')}}">Contact</a> --}}
                                        <span class="header_nav_arrow">
                                            <span class="icon is-arrow-down2"></span>
                                        </span>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        <li class="has-sub ml-xl-5">
                            <a class="sub-text-nav" href="#">Products</a>
                            <span class="header_nav_arrow">
                                <span class="icon is-arrow-down2"></span>
                            </span>
                            <div class="header_nav_sub">
                                <navbar-main></navbar-main>
                            </div>
                        </li>
                        @if (Auth::guest())
                        <li class=" ml-xl-5">
                            <a class="text-nav" href="#" data-toggle="modal" data-target="#loginModal">{{ __('Login') }}</a>
                        </li>
                        <li class="ml-xl-5">
                            <a class="text-nav" href="{{route('contract.create')}}" >Register</a>
                        </li>
                        @else
                        {{-- หลังlogin แล้ว --}}
                        <li class=" ml-xl-5">
                            <a class="text-nav" href="{{route('products')}}">Dashboard</a>
                        </li>
                        @endif
                    </ul>
                </nav>
                <div class="header_nav_toggle icon-navbar-mobile">
                    <span class="icon is-menu"></span>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="section section-top-blackgroud">
    <div class="container">
        <div class="section-top-blackgroud_inner">

        </div>
    </div>
</div> --}}
