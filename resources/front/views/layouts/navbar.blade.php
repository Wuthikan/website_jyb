
@if (Auth::guest())
<div class="header">
    <div class="header is-overlay" data-ntr-header>
        <div class="header_holder"></div>
        <div class="header_container">
            <div class="container">
                {{-- <div class="header_top">
                    <div class="header_menu">
                        <div class="header_menu_item">
                            <a class="header_menu_link" href="login-1.html">
                                <span class="header_menu_link_icon icon is-avatar"></span>
                                <span class="header_menu_link_text">Account</span>
                            </a>
                        </div>
                    </div>
                 </div> --}}
                <div class="">
                    <a class="header_logo logo-brand" href="home-1.html">
                        <img src="{{ asset('img/webp/logo.webp') }}" alt="msgyblogo">
                    </a>
                    <nav class="header_nav">
                        <a class="header_nav_close" href="#">
                            <span class="header_nav_close_text">Close</span>
                            <span class="header_nav_close_icon icon is-close"></span>
                        </a>
                        <ul>
                            <li class="has-sub ml-xl-5">
                                <a href="{{ url('/') }}" style="color:black">Home</a>
                            </li>
                            <li class="has-sub ml-xl-5">
                                <a href="#">Pages</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <ul>
                                        <li class="has-sub">
                                            <a href="{{route('about-brand')}}">About Brand</a>
                                            <span class="header_nav_arrow">
                                                <span class="icon is-arrow-down2"></span>
                                            </span>
                                        </li>
                                        <li><a href="{{route('member')}}">Team</a></li>
                                        <li class="has-sub">
                                            <a href="{{route('contact')}}">Contact</a>
                                            <span class="header_nav_arrow">
                                                <span class="icon is-arrow-down2"></span>
                                            </span>
                                        </li>

                                    </ul>
                                </div>
                            </li>
                            <li class="has-sub ml-xl-5">
                                <a href="#">Products</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <navbar-main></navbar-main>
                                </div>
                            </li>
                            <li class="has-sub ml-xl-5">
                                <a href="#">Account</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <ul>
                                        <li class="has-sub"><a href="{{route('contract.create')}}">Register</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal">{{ __('Login') }}</a></li>
                                        <li><a href="{{route('forgot-password')}}">Forgot Password</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <div class="header_nav_toggle">
                        <span class="icon is-menu icon-menu-home"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- หลังlogin แล้ว --}}
@else
<div class="header is-overlay" data-ntr-header>
    <div class="header_holder"></div>
    <div class="header_container">
        <div class="container">
            {{-- <div class="header_top">
                <div class="header_menu">
                    <div class="header_menu_item">
                        <a class="header_menu_link" href="login-1.html">
                            <span class="header_menu_link_icon icon is-avatar"></span>
                            <span class="header_menu_link_text">Account</span>
                        </a>
                    </div>
                </div>
            </div> --}}
            <div class="header_bottom">
                <a class="header_logo logo-brand" href="home-1.html">
                    <img src="{{ asset('img/webp/logo.webp') }}" alt="Naturally">
                </a>
                <nav class="header_nav">
                    <a class="header_nav_close" href="#">
                        <span class="header_nav_close_text">Close</span>
                        <span class="header_nav_close_icon icon is-close"></span>
                    </a>
                    <ul>
                        <li class="has-sub">
                            <a href="#">Home</a>
                        </li>
                        <li class="has-sub ml-xl-3">
                                <a href="#">Pages</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <ul>
                                        <li class="has-sub">
                                            <a href="{{route('about-brand')}}">About Brand</a>
                                            <span class="header_nav_arrow">
                                                <span class="icon is-arrow-down2"></span>
                                            </span>
                                        </li>
                                        <li><a href="{{route('member')}}">Team</a></li>
                                        <li class="has-sub">
                                            <a href="{{route('contact')}}">Contact</a>
                                            <span class="header_nav_arrow">
                                                <span class="icon is-arrow-down2"></span>
                                            </span>
                                        </li>

                                    </ul>
                                </div>
                            </li>
                            <li class="has-sub ml-xl-3">
                                <a href="#">Products</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <navbar-main></navbar-main>
                                </div>
                            </li>
                            <li class="has-sub">
                                <a href="#">บัตรตัวแทน</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                   <card-navbar></card-navbar>
                                </div>
                            </li>
                            <li class="has-sub">
                                <a href="#">ส่วนการเรียนรู้</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <ul>
                                        <li class="has-sub">
                                            <a href="#">อัลบัมรูป</a>
                                            <span class="header_nav_arrow">
                                                <span class="icon is-arrow-down2"></span>
                                            </span>
                                            <div class="header_nav_sub">
                                                <ul>
                                                    <li><a href="shop-1.html">สครับอโวคาโด้</a></li>
                                                    <li><a href="shop-2.html">คาโมมายด์</a></li>
                                                    <li><a href="shop-3.html">ครีมโสมสมุนไพร</a></li>
                                                    <li><a href="shop-4.html">มาร์กสครับแมคคาเดเมีย</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="shop-single-1.html">วิดีโอ</a></li>
                                    </ul>
                                </div>
                            </li>
                        <li class="has-sub ml-xl-5">
                                <a href="#">Account</a>
                                <span class="header_nav_arrow">
                                    <span class="icon is-arrow-down2"></span>
                                </span>
                                <div class="header_nav_sub">
                                    <ul>
                                        <li class="has-sub">
                                            <a class="header_menu_link" href="{{ url('/logout') }}">
                                                <span class="header_menu_link_icon icon is-avatar"></span>
                                                <span>Logout</span>
                                                {{-- <div class="row">
                                                    <span class="header_menu_link_icon icon is-avatar"></span>
                                                    <span class="header_menu_link_text"><a href="{{ url('/logout') }}"> logout </a></span>
                                                </div> --}}
                                            </a>
                                            <div class="header_menu_item">

                                            </div>
                                        </li>
                                        <li><a href="shop-single-1.html">บัตรตัวแทน</a></li>
                                        <li><a href="shop-single-1.html">Forgot Password</a></li>
                                    </ul>
                                </div>
                            </li>

                    </ul>
                </nav>
                <div class="header_nav_toggle">
                    <span class="icon is-menu"></span>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
