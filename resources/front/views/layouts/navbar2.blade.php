<section class="navbar-main pt-md-3 mt-md-4">
    <div class="container-md pr-md-0 p-sm-0">
        <div class="col-lg-12 p-md-0 p-sm-0 p-0">
            <nav class="navbar navbar-expand-md navbar-light pl-md-0 pr-md-0 p-sm-0 p-0 background-navbar">
                <a class="navbar-brand" href="#">
                    <img class="logo-brand" src="{{ asset('img/logo.png') }}"  alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link sarabun pl-sm-4 pl-2" href="{{ url('/') }}">about brand </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link sarabun pl-lg-5 ml-lg-5 ml-md-5 pl-md-4 pl-sm-4 pl-2" href="#">product</a>
                    </li>
                  </ul>
                  <li class="nav-item">
                    <a class="nav-link sarabun pr-lg-5 mr-lg-5 pl-sm-0 pl-sm-4 pl-2" href="{{route('member.index')}}"  aria-disabled="true">member</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link sarabun pl-sm-0 pl-sm-4 pl-2" href="{{route('contract.create')}}"  aria-disabled="true">Login/Register</a>
                  </li>
                </div>
              </nav>
        </div>
    </div>  
</section>

        



{{-- <section class="navbar-main">
    <div class="container">
        <div class="col-md-12 col-sm-12">
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item pt-4 mt-4">
                  <a class="nav-link active sarabun" href="#">about brand</a>
                </li>
                <li class="nav-item pt-4 mt-4">
                    <a class="nav-link sarabun" href="#" tabindex="-1" aria-disabled="true">
                        product
                    </a>
                </li>
                <li class="nav-item ">
                  <img src="{{ asset('img/logo.png') }}" alt="" class="logo-brand">
                </li>
                <li class="nav-item pt-4 mt-4">
                  <a class="nav-link sarabun" href="#" tabindex="-1" aria-disabled="true">member</a>
                </li>
                <li class="nav-item pt-4 mt-4">
                    <a class="nav-link sarabun" href="#" tabindex="-1" aria-disabled="true">Login/Register</a>
                  </li>
              </ul>
        </div>
    </div>
</section> --}}




{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        {{-- <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a> --}}
        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button> --}}

        
        {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->

                <ul class="navbar-nav mr-auto">
                </ul>

            

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link calltoaction" href="#">Submit your domain</a>
                </li>
            </ul>
        </div> --}}
    {{-- </div>
</nav>  --}}