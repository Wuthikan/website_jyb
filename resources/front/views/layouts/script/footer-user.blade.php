<!-- Scripts -->
<script src="{{ asset('js/app-user.js') }}" defer></script>

<!-- Jquery JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.min.js') }}" defer></script>
<!-- Jquery UI JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery-ui/jquery-ui.min.js') }}" defer></script>
<!-- Magnific Popup JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.magnific-popup.min.js') }}" defer></script>
<!-- Waypoints JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.waypoints.min.js') }}" defer></script>
<!-- countTo JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.countTo.js') }}" defer></script>
<!-- Slick Carousel JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/slick/slick.min.js') }}" defer></script>
<!-- imagesLoaded JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/imagesloaded.pkgd.min.js') }}" defer></script>
<!-- Isotope JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/isotope.pkgd.min.js') }}"></script>
<!-- Lazy Loading JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/lazysizes.min.js') }}" defer></script>
<!-- Animate on Scroll JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/aos.js') }}" defer></script>
<!-- Google Map JS -->
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI6l3aScGNBNG_7-lHZgnU9691uBCid8I"></script> --}}
<!-- Template JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/js/scripts.js') }}" defer></script>
<!-- Custom JS -->
<script src="{{ asset('naturally-template/naturally-html/html/assets/js/custom.js') }}" defer></script>