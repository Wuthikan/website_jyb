<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.min.js') }}" defer></script>   
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery-ui/jquery-ui.min.js') }}" defer></script>   
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.magnific-popup.min.js') }}" defer></script>   
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.waypoints.min.js') }}" defer></script>   
{{-- <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.countTo.js') }}" defer></script> --}}
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/slick/slick.min.js') }}" defer></script>
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/imagesloaded.pkgd.min.js') }}" defer></script>
{{-- <script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/isotope.pkgd.min.js') }}" defer></script> --}}
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/lazysizes.min.js') }}" defer></script>
<script src="{{ asset('naturally-template/naturally-html/html/assets/vendors/aos.js') }}" defer></script>
<script src="{{ asset('naturally-template/naturally-html/html/assets/js/scripts.js') }}" defer></script>
{{-- <script src="{{ asset('naturally-template/naturally-html/html/assets/js/custom.js') }}" ></script> --}}