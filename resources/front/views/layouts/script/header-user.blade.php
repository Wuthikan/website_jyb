<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name') }}-Thailand</title>
{{-- icon --}}
<link rel="apple-touch-icon" sizes="180x180" href="/icon/jyb-180x180.png">
<link rel="icon" type="image/png" sizes="32x32" href="/icon/jyb-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/icon/jyb-16x16.png">

{{-- template --}}
<link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/jquery.magnific-popup.css') }}" rel="stylesheet">
<!-- Slick Carousel CSS -->
<link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/slick/slick.css') }}" rel="stylesheet">
<link href="{{ asset('naturally-template/naturally-html/html/assets/vendors/aos.css') }}" rel="stylesheet">
<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('naturally-template/naturally-html/html/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('naturally-template/naturally-html/html/assets/css/custom.css') }}">
{{-- end-template --}}


<!-- Styles -->
<link href="{{ asset('css/front-user.css') }}" rel="stylesheet">