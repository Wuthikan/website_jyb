@extends('layouts.app-user')

@section('content')
<div class="section text-center">
    <div class="container" id="member-card-user">
        <div class="headline">
            <h1 class="headline_title">Dashboard</h1>
            <p>{{$brand->eng_name}}</p>
            <br>
        </div>
        <member-card-user :card="{{$card}}" :member="{{$member}}"></member-card-user>
    </div>
</div>
@endsection
