@extends('layouts.app-user')

@section('content')
<div class="section" id="total-member">
    <div class="container">
        <div class="headline  text-center">
           <h1 class="headline_title">สายงานทั้งหมด</h1>
           <p class="headline_summary">{{$brand->eng_name}}</p>
        </div>
        <total-member :brand_id="{{$brand->id}}" :auth_id="{{Auth::user()->id}}"></total-member>
    </div>
</div>
@endsection
