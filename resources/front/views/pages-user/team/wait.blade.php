@extends('layouts.app-user')

@section('content')
<div class="section text-center">
    <div class="container">
        <div class="headline mb-5">
            <h1 class="headline_title ">สมาชิกที่รอดำเนิการ</h1>
            <p class="headline_summary">{{$brand->eng_name}}</p>
        </div>
        <div class="approve" id="approve-user-register">
            <approve-users :authmember="{{$member}}" :brandid="{{$brand->id}}"></approve-users>
        </div>
    </div>
</div>

@endsection
