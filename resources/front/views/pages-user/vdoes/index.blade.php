@extends('layouts.app-user')

@section('content')
<div class="section background-contain-variable mb-200" id="show-list-vdo-train">
    <div class="container">
        <div class="headline">
            <div class="section-elements_heading text-center">
                <h3>Videos Gallery</h3>
                <p>{{$brand->eng_name}}</p>
            </div>
        </div>
        <show-list-vdo-train :brand="{{$brand}}" ></show-list-vdo-train>
    </div>
</div>
@endsection
