@extends('layouts.app-user')

@section('content')
<div class="section text-center">
    <div class="container">
        <div class="headline">
            <div id="vdo-train">
                <vdo-train :brand="{{$brand}}" :vdo="{{$vdo}}"></vdo-train>
            </div>
        </div>
    </div>
</div>
@endsection
