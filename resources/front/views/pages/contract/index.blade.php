@extends('layouts.app')

@section('content')
    <div class="section section-top-blackgroud">
        <div class="container">
            <div class="section-top-blackgroud_inner">

            </div>
        </div>
    </div>
    <div class="section" data-ntr-blog-single>
		<b-container fluid="sm">
			<!-- Single Header -->
			<div class="blog-single_header margin-header" data-aos="ntr-fade-up" data-aos-once="true">
				<h1 class="blog-single_title">สัญญาตัวแทนจำหน่ายสินค้า</h1>
				<ul class="blog-single_meta">
					<li class="blog-single_meta_item">
						<span class="blog-single_meta_item_icon icon is-calendar"></span>
						<span>January 1, 2021</span>
					</li>
					<li class="blog-single_meta_item">
						<span class="blog-single_meta_item_icon icon is-user"></span>
						<span>By <a href="#">CEO MSGYB</a></span>
					</li>
				</ul>
			</div>
			<!-- Single Header End -->
			<!-- Single Media -->
			<div class="blog-single_media" data-aos="ntr-fade-up" data-aos-once="true" data-aos-delay="200">
				<div class="blog-single_media_quote">
				</div>
			</div>
		</b-container>
	</div>
    <section class="contract contract-box mt-5 pt-5">
        <b-container fluid="sm">
            <div class="row">
                <!-- Grid column -->
                <div class="col-md-2"></div>
                <div class="col-md-8 mb-4">
                <!-- Exaple 1 -->
                <div class="card example-1 scrollbar-dusty-grass box-shadow contract-box-body" >
                    {{-- <div class="card-header contract-box-header justify-content-center">
                        <h1 id="" class="text-center sarabun"><strong><img class="contract-logo" src="{{ asset('img/logo.png') }}"  alt="">สัญญาตัวแทนจำหน่ายสินค้า</strong></h1>
                        <ul class="blog-single_meta">
                            <li class="blog-single_meta_item">
                                <span class="blog-single_meta_item_icon icon is-calendar"></span>
                                <span>April 16, 2019</span>
                            </li>
                            <li class="blog-single_meta_item">
                                <span class="blog-single_meta_item_icon icon is-user"></span>
                                <span>By <a href="#">Bobbie Ryan</a></span>
                            </li>
                        </ul>
                    </div> --}}
                    <div class="card-body">
                    <li class="text-center pb-5"><img class="contract-logo justify-content-center" src="{{ asset('img/logo.webp') }}" alt=""></li>
                    <li class="text-right sarabun contract-text pb-md-2">ทำที่ ห้างหุ้นส่วน จีวายบี เทรดดิ้ง จำกัด</li>
                        <li class="card-text sarabun contract-text pl-md-5 pr-md-5 pb-md-2">
                            &emsp;&emsp;&emsp;สัญญาฉบับนี้ทำขึ้นระหว่าง ห้างหุ้นส่วน จีวายบี เทรดดิ้ง จำกัด หรือ เอ็ม เอส กิ๊บ แบรนด์ ตั้งอยู่
                            เลขที่ 103/1 หมู่ที่3 บ้านสุพรรณ ซอยสุขสมบูรณ์ ตำบลชมภู อำเภอสารภี จังหวัดเชียงใหม่ 50140
                            ซึ่งต่อไปนี้ในสัญญานี้เรียกว่า “ตัวแทน” อีกฝ่ายหนึ่ง ทั้งสองฝ่ายตกลงทำสัญญากันมีข้อความ ดังต่อไปนี้
                                โดยที่ ห้างหุ้นส่วน จีวายบี เทรดดิ้ง จำกัด หรือ เอ็ม เอส กิ๊บ แบรนด์ เป็นเจ้าของผลิตภัณฑ์ เห็นเป็นการจำเป็น และสมควรจำหน่ายผลิตภัณฑ์สินค้าในเครือเอ็ม เอส กิ๊บ แบรนด์ เพื่อเป็นการส่งเสริมกิจการให้เจริญก้าวหน้า ทางแบรนด์
                            จึงแต่งตั้ง “ตัวแทน” โดยทั้งสองฝ่ายเห็นสอดคล้องกัน จึงได้กำหนดเงื่อนไขไว้โดยความยินยอมของทั้งสองฝ่าย ดังต่อไปนี้
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 1. ตัวแทนตกลงยอมรับจะเป็นตัวแทนจำหน่ายผลิตภัณฑ์สินค้าในเครือ เอ็ม เอส กิ๊บ แบรนด์ โดยไม่มีกำหนดระยะเวลา ด้วยความซื่อสัตย์สุจริต อีกทั้งจะใช้ความรู้ความสามารถที่จะส่งเสริมสินค้าของ “เจ้าของผลิตภัณฑ์” ให้เป็นที่แพร่หลาย และให้คำรับรองและสัญญาว่าจะไม่กระทำการอย่างหนึ่งอย่างใดให้เป็นการเสื่อมเสียและกระทบกระเทือนกับการจำหน่ายผลิตภัณฑ์ดังกล่าว
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 2. “เจ้าของผลิตภัณฑ์” จะเป็นผู้กำหนดราคาของสินค้าตามราคาที่ตั้งไว้ตั้งแต่ต้นของผลิตภัณฑ์นั้น
                            และจะกำหนดราคาโปรโมชั่นให้ตัวแทนทราบเป็นครั้งคราว โดยตัวแทนจะต้องไม่จำหน่ายสินค้าต่ำไปกว่าราคาที่กำหนด
                            &emsp; และจะต้องเรียกเก็บค่าจัดส่งจากผู้ซื้อตามที่ “เจ้าของผลิตภัณฑ์” ได้กำหนดไว้ หากตัวแทนกระทำการฝ่าฝืนเงื่อนไข ในเรื่องของราคาผลิตภัณฑ์หรือราคาค่าจัดส่งสินค้าดังกล่าว ทาง “เจ้าของผลิตภัณฑ์” สามารถดำเนินคดีในทางอาญา หรือฟ้องเรียกค่าสินไหมทดแทนในทางแพ่งกับตัวแทนจำหน่ายสินค้านั้นได้ และยังสามารถตัดสิทธิการเป็นตัวแทนตำหน่ายนั้นได้ทันที
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 3. เจ้าของผลิตภัณฑ์ไม่อนุญาตให้ตัวแทนทำการจำหน่ายผลิตภัณฑ์นั้นในราคาตัวแทนจำหน่ายให้แก่ผู้ซื้อ
                            และไม่สามารถจัดโปรโมชั่นลด แลก แจก แถม แก่ผู้ซื้อ หรือการกระทำใด อันเป็นการกระทบต่อราคาผลิตภัณฑ์โดยปราศจากการยินยอมของเจ้าของผลิตภัณฑ์
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 4. ตัวแทนสามารถจำหน่ายสินค้าผ่านช่องทางช้อปปิ้งออนไลน์ อาทิเช่น SHOPEE LAZADA หรือเว็บไซต์ออนไลน์อื่นใดประเภทเดียวกันในระบบอินเตอร์เน็ตออนไลน์ได้ โดยจะต้องจำหน่ายในราคาที่เจ้าของผลิตภัณฑ์กำหนดเท่านั้น ห้ามเข้าร่วมโปรโมชั่นของแหล่งช้อปปิ้งออนไลน์หรือเว็บไซต์ช้อปปิ้งออนไลน์โดยเด็ดขาด เว้นแต่ได้รับอนุญาตจากเจ้าของผลิตภัณฑ์
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 5. ตัวแทนจำหน่ายไม่สามารถรับลูกทีมหรือตัวแทนจำหน่ายช่วงต่อจากตนเองได้ เว้นแต่เจ้าของผลิตภัณฑ์ได้กำหนดไว้ให้ดำรงตำแหน่ง “แม่ทีม” เท่านั้น และต้องมีบัตรประจำตัวเป็นแม่ทีมเท่านั้นที่จะสามารถรับตัวแทนจำหน่ายได้
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 6. ตัวแทนไม่สามารถเปลี่ยนสายงานหรือเปลี่ยนแม่ทีมเองได้ รวมถึงไม่สามารถสั่งสินค้าข้ามสายงานได้ เว้นแต่กรณีแม่ทีมนั้นได้ทำการเลิกขายผลิตภัณฑ์ หรือแม่ทีมไม่มีประสิทธิภาพในการดูแลลูกทีม โดยจะต้องผ่านการยินยอมของเจ้าของผลิตภัณฑ์ในการย้ายสายงานจึงจะเป็นผล
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 7. ตัวแทนจำหน่ายจะต้องเติมสินค้าภายในระยะเวลาไม่ต่ำกว่า 3 เดือน และหากพบว่าตัวแทนจำหน่ายไม่ได้เติมสินค้าภายในระยะเวลาที่กำหนด หรือไม่มีความเคลื่อนไหวภายในระยะเวลา 1 เดือน ทางเจ้าของผลิตภัณฑ์มีสิทธิดำเนินการตัดสิทธิการเป็นตัวแทนจำหน่ายได้ทันที
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 8. การเป็นแม่ทีมจะต้องมีประสิทธิภาพและสามารถแก้ไขปัญหาที่เกิดระหว่างลูกทีมที่ทำผิดเงื่อนไขได้ กล่าวคือหากปรากฏว่าแม่ทีมคนใดทำการเพิกเฉยต่อตำแหน่งแม่ทีม อาทิเช่น ลูกทีมไม่สามารถติดต่อได้ ส่งของไม่ตรงตามที่กำหนด หรือการกระทำอื่นที่บ่งบอกถึงการละเลยตำแหน่งแม่ทีม แม่ทีมคนดังกล่าวจะถูกตัดสิทธิของการเป็นแม่ทีมทันที
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 9. การเป็นแม่ทีมจะต้องสต๊อกของตามที่เจ้าของผลิตภัณฑ์ได้กำหนดไว้ ตามเรทจำนวนของราคาผลิตภัณฑ์
                            และต้องเติมสต๊อกภายในระยะเวลาไม่ต่ำกว่า 3 เดือน และต้องมีจำนวนสินค้าในสต๊อกจำนวนมากกว่าลูกทีม กล่าวคือหากปรากฎว่าแม่ทีมคนใดมีสินค้าไว้จำนวนน้อยกว่าลูกทีมจะถูกปรับลดตำแหน่งลงมา
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 10. หากตัวแทนไม่ประสงค์จะจำหน่ายผลิตภัณฑ์แล้ว ต้องดำเนินการติดต่อเจ้าของผลิตภัณฑ์ หรือแม่ทีม เพื่อทำการยกเลิกสัญญาตัวแทนจำหน่ายฉบับนี้ทันที หรือในกรณีที่ตัวแทนกระทำการฝ่าฝืนข้อสัญญาข้อหนึ่งข้อใดในสัญญาฉบับนี้ การเป็นตัวแทนจำหน่ายจะสิ้นสุดลงทันที
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 11. หากตัวแทนยังมีผลิตภัณฑ์ในครอบครองจะถือว่าสัญญาฉบับนี้ยังไม่สิ้นสุดลง จนกว่าตัวแทนจะแจ้งยกเลิกสัญญากับเจ้าของผลิตภัณฑ์ กล่าวคือตัวแทนจะต้องจำหน่ายสินค้าให้หมดมือจึงจะถือว่าสัญญานี้สิ้นสุดลง รวมถึงไม่สามารถขอคืนผลิตภัณฑ์กับแม่ทีมหรือเจ้าของผลิตภัณฑ์ได้
                        </li>
                        <li class="sarabun contract-text pl-md-5 pr-md-5">
                            &emsp;&emsp;&emsp;ข้อ 12. กรณีที่เมื่อมีการยกเลิกสัญญา หรือ สิ้นสุดการเป็นตัวแทนจำหน่ายสินค้า ตัวแทนจะต้องทำการลบรูปภาพหรือสิ่งใดที่เกี่ยวข้องกับการขายผลิตภัณฑ์ทั้งหมด หากในภายหลังเจ้าของผลิตภัณฑ์ได้ตรวจสอบแล้วพบว่าไม่ดำเนินการดังกล่าว เจ้าของผลิตภัณฑ์มีสิทธิเรียกค่าเสียหายเป็นจำนวน 10 เท่าของราคาผลิตภัณฑ์
                        </li>
                        <li class="text-center mt-md-3"></li>
                        <hr>
                        <div class="form-group pl-md-5 mb-5 text-center">
                            <div class="form-check">
                                {{-- <input type="checkbox" id="checkme" onclick="onchange()" />
                                <label class="form-check-label" for="invalidCheck"></label>
                                <button class="btn btn-success ml-md-2" id="submitContract"

                                onclick="window.location='{{ route("register") }}'">
                                    ยอมรับเงื่อนไขในสัญญา
                                </button> --}}
                                <disabled-button></disabled-button>
                            </div>
                        </div>
                        <br>
                          {{-- <button class="btn btn-primary ml-md-5"><a class="btn-contract-submit" href="{{route('register.create')}}" >ต่อไป</a></button> --}}
                    </div>
                </div>
                <!-- Exaple 1 -->
                </div>
                <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </b-container>
    </section>

@endsection
@section('script')
    <script>
    </script>
@endsection

