@extends('layouts.app-register')

@section('content')
<!-- Section Headline -->
<div class="section section-custom-register-brands">
	<div class="container">
		<!-- Headline -->
		<div class="headline mb-5">
			<img src="/image/public/brands/default/logo2-brand-defaul-image.png" alt="About us">
			<p class="headline_summary">กรุณาเลือกแบรนด์</p>
		</div>
	</div>
<!-- Section Headline End -->
	<div class="container">
		<div class="grid">
			<div class="grid_row is-40">
				@foreach ($brands as $brand)
				<div class="grid_col is-12" data-aos="ntr-fade-up" data-aos-once="true">
					<!-- Post -->
					<div class="blog-post2 card-product-select">
						<a class="blog-post2_photo" href="/{{$brand->slug}}/{{$brand->id}}/dashboard">
							<span class="aspect-ratio is-1x1">
								<img class="aspect-ratio_object lazyload" 
									src='{{$brand->path_img}}'
									alt="{{ $brand->eng_name }}">
							</span>
						</a>
						<div class="blog-post2_info">
							<h3 class="blog-post2_title"><a href="/{{$brand->slug}}/{{$brand->id}}/dashboard">{{ $brand->name }}</a></h3>
						<p class="blog-post2_meta"><a href="/{{$brand->slug}}/{{$brand->id}}/dashboard">{{ $brand->eng_name }}</a></p>
							<p class="blog-post2_summary">{{ $brand->desc }}</p>
						</div>
					</div>
					<!-- Post End -->
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection
