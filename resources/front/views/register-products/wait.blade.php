@extends('layouts.app-register')

@section('head')
@endsection

@section('content')
<div class="section" id="page-register-products-wait" data-ntr-shop-single>
    <div class="container">
        <div class="grid">
            <div class="grid_row is-60 mb-4">
                <div class="grid_col is-12 is-lg-12 text-center">
                    <div class="headline">
                        <h1 class="headline_title">{{$brand->eng_name}}</h1>
                        <p class="headline_summary">{{$brand->name}}</p>
                    </div>
                </div>
            </div>
            <div class="grid_row is-60">
                <div class="grid_col is-12 is-lg-5">
                    <div class="shop-single_photos bg-white" data-ntr-lightbox='{"type":"gallery","selector":".shop-single_photos_item"}'>
                        <div class="slick-slider">
                            <!-- รูปหลัก -->
                            @foreach ($brand->brandImage as $show_img)
                            <div class="slick-slide">
                                <a class="shop-single_photos_item" href="{{$show_img->path}}" title="Product Name">
                                    <span class="aspect-ratio is-1x1">
                                        <img 
                                        class="aspect-ratio_object lazyload"
                                        src="/image/public/default/0/blank.gif" 
                                        data-srcset="{{$show_img->path}} 1x, {{$show_img->path}} 2x" 
                                        alt="Product Name">
                                    </span>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="shop-single_thumbnails">
                        <div class="slick-slider" >
                            @foreach ($brand->brandImage as $show_img)
                            <div class="slick-slide">
                                <div class="shop-single_thumbnails_item bg-white">
                                    <span class="aspect-ratio is-1x1">
                                        <img class="aspect-ratio_object lazyload" 
                                            src="/image/public/default/0/blank.gif" 
                                            data-srcset="{{$show_img->path}} 1x, {{$show_img->path}} 2x" 
                                            alt="Product Name">
                                    </span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="grid_col is-12 is-lg-7" >
                    <register-brand-wait :adviser_data="{{$member}}" ></register-brand-wait>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection