@extends('layouts.app')

@section('content')
<!-- Section Hero -->
<div class="section section-hero">
    <div class="container">
        <div class="section-hero_inner">
            <div class="section-hero_heading" data-aos="ntr-fade-up" data-aos-once="true">
                <h1 class="section-hero_heading_title">Msgyb Thailand</h1>
                <p class="section-hero_heading_text">
                    แบรนด์มิสกิ๊บ หรือ เอ็ม เอส กิ๊บ เป็นแบรนด์สกินแคร์ธรรมชาติที่ผลิตมา
                </p>
                <p  class="section-hero_heading_text">
                    <b>“เพื่อผู้ที่มีผิวแพ้ง่ายโดยเฉพาะ”</b>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Section Product 1 -->
<div class="section-product-1">
    <div class="container">
        <div class="row d-flex flex-sm-column-reverse flex-md-row reverse-xs" data-ntr-slider-deals>
            <div class="col-sm-12 col-md-7 col-lg-7" data-aos="ntr-fade-up" data-aos-once="true">
                <h2 class="content-deals_title">THAI HEBAL BODY CREAM</h2>
                <p class="content-deals_summary">
                    ครีมโสมสมุนไพร
                    สารสกัดจากสมุนไพรไทยดั้งเดิม รวมสมุนไพรตัวท็อป
                    ประกอบด้วย โสมดำ มะหาด ไพล รากชะเอม
                    และน้ำมันมะกอก มาพร้อมกลิ่นผ่อนคลาย
                    ช่วยบำรุงผิวเพื่อผิวเปล่งประกาย ลดเลือนรอยดำ
                    และขาวกระจ่างใสอย่างเป็นธรรมชาติ
                </p>
                <p>
                    <a class="link is-color-secondary" href="shop-1.html">
                        <span class="link_text">Shop Now</span>
                        <span class="link_icon icon is-next"></span>
                    </a>
                </p>
            </div>
            <div class="col-sm-12 col-md-5">
                <div class="product-box">
                    <img src="{{asset('img/webp/th-herbal.webp')}}" class="lazyload" alt="thai-herbal">
                    <div class="text-image">ครีมโสมสมุนไพร</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection