@extends('layouts.app-register')

@section('content')
	<!-- Section Headline -->
	<div class="section section-custom-register-brands">
		<div class="container" id="change-password">
			<!-- Headline -->
			<div class="headline">
                {{-- <h1 class="headline_summary">Change Password</h1> --}}
                <change-password></change-password>
			</div>
		</div>
	</div>
@endsection
