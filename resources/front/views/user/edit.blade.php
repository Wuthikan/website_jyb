@extends('layouts.app-register')

@section('content')
	<!-- Section Headline -->
	<div class="section section-custom-register-brands" id="edit-user-profile">
		<div class="container">
			<div class="grid">
				<div class="grid_row is-40 mb-4">
					<div class="grid_col is-12">
						<div class="headline">
							<h1 class="headline_title">Profile</h1>
							<p class="headline_summary">แก้ไขข้อมูลส่วนตัว</p>
						</div>
					</div>
				</div>
				<div class="grid_row is-40">
					<edit-user-profile></edit-user-profile>
				</div>
			</div>
		</div>
	</div>
@endsection
