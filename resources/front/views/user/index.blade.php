@extends('layouts.app-register')

@section('content')
	<!-- Section Headline -->
	<div class="section section-custom-register-brands" id="user-profile">
		<div class="container">
			<div class="grid">
				<div class="grid_row is-40 mb-4">
					<div class="grid_col is-12">
						<div class="headline" id="user-profile">
							<h1 class="headline_title">Profile</h1>
							<p class="headline_summary">ข้อมูลส่วนตัว</p>
						</div>
					</div>
				</div>
				<div class="grid_row is-40">
					<user-profile></user-profile>
				</div>
			</div>
		</div>
	</div>
@endsection
