@extends('layouts.app')

@section('content')
    <div class="container-fluid background-header">
        <h1 class="msgyb sarabun">MSGYB THAILAND</h1>
        <h3 class="detail-company sarabun">
            แบรนด์มิสกิ๊บ หรือ เอ็ม เอส กิ๊บ เป็นแบรนด์สกินแคร์ธรรมชาติที่ผลิตมา <br>
            <p class="sarabun fs-3">“เพื่อผู้ที่มีผิวแพ้ง่ายโดยเฉพาะ”</p>
            {{-- <p class="detail-company sarabun" style="font-size:1.2em; padding-top:1em;">“เพื่อผู้ที่มีผิวแพ้ง่ายโดยเฉพาะ”</p> --}}
            <h3 class="detail-company d-none d-sm-block sarabun">การคิดสูตรเฉพาะภายใต้ผลิตภัณฑ์ของแบรนด์ <br>
                ได้นำสารสกัดคุณภาพสูงจากธรรมชาติ <br>
                เน้นให้ผู้ใช้สัมผัสถึงเนื้อและสารสกัดจริง <br>
                ผลิตภัณฑ์ทุกชิ้นที่ผลิตภายใต้แบรนด์เราปราศจาก <br>
                การแต่งสีสังเคราะห์หรือแต่งกลิ่นน้ำหอมใดๆ <br>
                เน้นการใส่สารสกัดเปอร์เซ็นต์ที่สูงและ <br>
                เน้นผลลัพธ์ที่ดีที่สุดเพื่อผิวแพ้ง่าย
            </h3>
        </h3>
    </div>
    <div class="background-content">
        <div class="container-fluid">
            {{-- ครีมโสมสมุนไพร --}}
            <div class="row row-thai-herbal">
                <div class="col-sm-12 col-md-12 bg-main bg-main-height-sec1 z-index-1 lazyload pt-detail-mobile"></div>
                <div class="col-md-12 thai-herbal-box p-sm-0">
                    <div class="container p-0 p-md-0 p-sm-0">
                        <div class="row d-flex flex-sm-column-reverse flex-md-row reverse-xs">
                            <div class="col-sm-12 col-md-7 col-lg-7 p-sm-0 ">
                                <h1 class="thai-herbal-head sarabun">ครีมโสมสมุนไพร</h1>
                                <h3 class="thai-herbal-detail sarabun">
                                    ครีมโสมสมุนไพร <br>
                                    สารสกัดจากสมุนไพรไทยดั้งเดิม รวมสมุนไพรตัวท็อป <br>
                                    ประกอบด้วย โสมดำ มะหาด ไพล รากชะเอม <br>
                                    และน้ำมันมะกอก มาพร้อมกลิ่นผ่อนคลาย <br>
                                    ช่วยบำรุงผิวเพื่อผิวเปล่งประกาย ลดเลือนรอยดำ <br>
                                    และขาวกระจ่างใสอย่างเป็นธรรมชาติ <br>
                                </h3>
                            </div>
                            <div class="col-sm-12 col-md-5 image-thai-herb-box">
                                <img src="{{asset('img/webp/th-herbal.webp')}}" class="thai-herbal-image lazyload" alt="thai-herbal">
                                <div class="thai-herbal-listband sarabun">ครีมโสมสมุนไพร</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            {{-- end --}}
        <div class="container-fluid">
            {{-- Avocado --}}
            <div class="row row-avocado">
                <div class="col-md-1"><div></div></div>
                {{-- background green --}}
                <div class="col-md-11 bg-main bg-main-height-sec2 mt-md-60 mt-lg-120 lazyload" style="z-index: -1;"></div>
                {{-- <div class="col-sm-12 col-md-12 bg-main bg-main-height-sec2 z-index-1 lazyload"></div> --}}
                {{-- **** --}}
                <div class="col-md-12 avo-box p-sm-0">
                    <div class="container-sm padding-contain-avo p0-avo">
                        <div class="row box-shadow-avo mg0-avo-mobile">
                            {{-- **** --}}
                            <div class="col-12 col-md-5 avocado-image-box">
                                <img src="{{asset('img/webp/avo.webp')}}" class="avocado-image w-100 lazyload" alt="avocado">
                                <div class="avocado-listband sarabun">สครับอโวคาโด้</div>
                            </div>
                            <div class="col-12 col-md-7 col-lg-7 bg-white pl-md-0">
                                <h1 class="avocado-head sarabun pl-lg-60">Avocado</h1>
                                <h3 class="avocado-detail sarabun pl-lg-60 pl-md-30 pt-lg-30">
                                    สครับอโวคาโด้สูตรใหม่ เน้นในเรื่องของผิวขาวกระจ่างใส<br>
                                    ลดเลือนรอยดำ สมานผิวให้เรียบเนียน<br>
                                    ช่วยผลัดเซลล์ผิวอย่างอ่อนโยนโดยไม่ทำให้ผิวบาง<br>
                                    ผสานด้วยเมล็ดสครับจากเปลือกอโวคาโด้และเมล็ดคาเคา<br>
                                    เนื้อสครับนุ่มฟู ถูแล้วแตกตัวเป็นน้ำนม<br>
                                    สามารถขัดได้ทุกวัน โดยไม่ทำให้ผิวบาง
                                </h3>
                            </div>
                            {{-- **** --}}
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-1"></div> --}}
            </div>
            {{-- ****** --}}
        </div>
        <div class="container-fluid">
            {{-- camomild --}}
            <div class="row row-camomild">
                <div class="col-md-12 bg-main2 bg-main-height-sec3 lazyload padding-bg-green-camo" style="z-index: -1;"></div>
                <div class="col-md-12 p-sm-0 camo-row-position">
                    <div class="container-sm padding-contain-avo p-iphone-0">
                        <div class="row d-flex flex-sm-column-reverse flex-md-row reverse-xs box-shadow-avo mg0-avo-mobile">
                            <div class="col-md-7 col-lg-7 bg-white pl-md-0 pt-90">
                                <h1 class="avocado-head sarabun pl-md-60">น้ำตบคาโมมายล์</h1>
                                <h3 class="avocado-detail sarabun">
                                    น้ำตบคาโมมายล์ x คาเลนดูล่า<br>
                                    สารสกัดหลักจากดอกคาโมมายล์และคาเลนดูล่า<br>
                                    พร้อมทั้งไฮยารูลอนเข้มข้น และวิตามินซีบริสุทธิ์<br>
                                    ปลอบประโลมผิว เพื่อผิวแข็งแรงสุขภาพดี<br>
                                    ยืนหนึ่งเรื่องสิวผด ผลิตมาเพื่อผิวแพ้ง่ายโดยเฉพาะ<br>
                                </h3>
                            </div>
                            <div class="col-md-5 col-lg-5 avocado-image p-md-0">
                                <img src="{{asset('img/webp/camo.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                                <div class="camo-listband sarabun">น้ำตบคาโมมายล์</div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- *********** --}}
                {{-- macadamia --}}
                <div class="col-md-12 p-sm-0 margin-macadamia">
                    <div class="container-sm padding-contain-avo p-iphone-0">
                        <div class="row d-flex flex-sm-column-reverse flex-md-row reverse-xs box-shadow-avo camo-row-position mg0-avo-mobile">
                            <div class="col-md-7 col-lg-7 pl-md-0 pt-90">
                                <h1 class="avocado-head sarabun pl-md-60">มาร์กสครับแมคคาเดเมีย</h1>
                                <h3 class="avocado-detail sarabun">
                                    สครับที่สามารถเป็นได้ทั้งมาส์กและสครับใน 1 เดียว<br>
                                    มีเม็ดสครับเป็นเมล็ดวอลนัทอยู่ในตัว<br>
                                    ผลัดเซลล์ผิวอย่างอ่อนโยน ช่วยฟื้นฟูผิวแห้งกร้าน<br>
                                    ผิวขาดน้ำพร้อมเติมน้ำให้ผิว เผยผิวเนียนเด้งน่าสัมผัส<br>
                                    ลดรอยสิวต่างๆ พร้อมทั้งกระชับรูขุมขนอีกด้วย<br>
                                    ทำผิวให้เปียก นวดวนและทิ้งไว้ 10 นาที<br>
                                </h3>
                            </div>
                            <div class="col-md-5 col-lg-5 avocado-image p-md-0">
                                <img src="{{asset('img/webp/cacao.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                                <div class="camo-listband sarabun">Avocao X Cacao</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="container-fluid footer-image lazyload pb-0"></div>
@endsection
