@extends('layouts.app')

@section('content')
<section class="section-slider-data">
    <div class="section section-custom-15">
		<!-- Slider Hero -->
		<div class="slider-hero" data-ntr-slider-hero>
			<div class="slick-slider">
				<div class="slick-slide">
					<div class="slider-hero_item lazyload" data-ntr-background="{{asset('img/webp/slider/1.webp')}}">
						<div class="container">
							<div class="slider-hero_item_heading">
								<h1 class="slider-hero_item_title">สครับอโวคาโด้</h1>
								<p class="slider-hero_item_summary">Your journey to a healthier life starts here.</p>
								<p>
									<a class="link is-color-secondary" href="#">
										<span class="link_text">Shop Now</span>
										<span class="link_icon icon is-next"></span>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="slick-slide">
					<div class="slider-hero_item lazyload" data-ntr-background="{{asset('img/webp/slider/2.webp')}}">
						<div class="container">
							<div class="slider-hero_item_heading">
								<h1 class="slider-hero_item_title">ครีมโสมสมุนไพร</h1>
								<p class="slider-hero_item_summary">Special offers all fruit products</p>
								<p>
									<a class="link is-color-secondary" href="#">
										<span class="link_text">Shop Now</span>
										<span class="link_icon icon is-next"></span>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="slick-slide">
					<div class="slider-hero_item lazyload" data-ntr-background="{{asset('img/webp/slider/3.webp')}}">
						<div class="container">
							<div class="slider-hero_item_heading">
								<h1 class="slider-hero_item_title">น้ำตบคาโมมายล์</h1>
								<p class="slider-hero_item_summary">Special offers this week</p>
								<p>
									<a class="link is-color-secondary" href="#">
										<span class="link_text">Shop Now</span>
										<span class="link_icon icon is-next"></span>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Slider Hero End -->
	</div>
</section>
้<section class="about-brand" style="margin-top:50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-4 pr-lg-0 mt-sm-5 ">
                        <img src="{{asset('img/webp/1.webp')}}" class="img-about-back lazyload" alt="Responsive image">
                    </div>
                    <div class="col-8 pr-lg-0 pl-lg-0 mt-sm-3">
                        <img src="{{asset('img/webp/2.webp')}}" class="img-about-front lazyload" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class=" pt-lg-3">
                    <li class="d-flex justify-content-center">
                        <img src="{{asset('img/webp/pronto.webp')}}" alt="" class="about-heart lazyload">
                    </li>
                   <h1 class="text-center sarabun aboutBrand-title mt-sm-5 mt-md-3">A B O U T &nbsp; B R A N D</h1>
                       <div class="text-center sarabun aboutBrand-text">
                            <li>แบรนด์มิสกิ๊บ หรือ เอ็ม เอส กิ๊บ</li>
                            <li>เป็นแบรนด์สกินแคร์ธรรมชาติที่ผลิตมา</li>
                            <li>“เพื่อผู้ที่มีผิวแพ้ง่ายโดยเฉพาะ”</li>
                       </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4 border-bottom-about1 pt-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-brand2 mt-md-5 pt-md-5 mt-sm-5 margin-bottom-lg-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pl-md-0">
                <li class="sarabun aboutBrand-text text-center mt-xl-2 mt-lg-2 pr-md-0 pl-md-3">
                    การคิดสูตรเฉพาะภายใต้ผลิตภัณฑ์ของแบรนด์
                    ได้นำสารสกัดคุณภาพสูงจากธรรมชาติ
                    เน้นให้ผู้ใช้สัมผัสถึงเนื้อและสารสกัดจริง
                    ผลิตภัณฑ์ทุกชิ้นที่ผลิตภายใต้แบรนด์เราปราศจาก
                    การแต่งสีสังเคราะห์หรือแต่งกลิ่นน้ำหอมใดๆ
                    เน้นการใส่สารสกัดเปอร์เซ็นต์ที่สูงและ
                    เน้นผลลัพธ์ที่ดีที่สุดเพื่อผิวแพ้ง่าย
                </li>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6 border-right-about2 mt-lg-3 mt-md-3 mb-sm-5 mb-5"></div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8 col-12 pr-lg-0">
                        <img src="{{asset('img/webp/4.webp')}}" class="img-about-front-sec2 lazyload" alt="Responsive image">
                    </div>
                    <div class="col-4 pl-0">
                        <img src="{{asset('img/webp/3.webp')}}" class="img-about-back-sec2 mt-lg-5 mt-md-4 lazyload" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-avocado pt-5 mt-5 margin-bottom-lg-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 p-0">
                <div class="img-avocado-header-box">
                    <img src="{{asset('img/27.jpg')}}" class="img-avocado-header" alt="Responsive image">
                </div>
                <img src="{{asset('img/23.jpg')}}" class="img-intro-avo" alt="Responsive image">
            </div>
            <div class="col-lg-7 col-md-7 pl-md-0 pr-sm-0 mb-sm-5 p-0">
                <img src="{{asset('img/webp/22.webp')}}" class="img-intro-background-avo lazyload" alt="Responsive image">
                <div class="intro-box-text-right-avo ml-sm-3">
                    <h1 class="text-right sarabun pt-xl-3 pt-lg-3 pt-md-3 pr-md-3 pr-1 pt-3 p-2
                    intro-text-header"><i>สครับอโวคาโด้ &nbsp;X&nbsp; คาเคา</i></h1>
                    <li class="text-right intro-text sarabun pl-lg-5 pr-lg-3 ml-lg-5 pr-md-3 ml-md-2 pt-md-0 pt-2">
                        สครับอโวคาโด้สูตรใหม่ เน้นในเรื่องของผิวขาวกระจ่างใส<br>
                        ลดเลือนรอยดำ สมานผิวให้เรียบเนียน<br>
                        ช่วยผลัดเซลล์ผิวอย่างอ่อนโยนโดยไม่ทำให้ผิวบาง<br>
                        ผสานด้วยเมล็ดสครับจากเปลือกอโวคาโด้และเมล็ดคาเคา<br>
                        เนื้อสครับนุ่มฟู ถูแล้วแตกตัวเป็นน้ำนม<br>
                        สามารถขัดได้ทุกวัน โดยไม่ทำให้ผิวบาง
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-ginseng mt-5 margin-bottom-lg-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 p-0">
                <img src="{{asset('img/webp/9.webp')}}" class="img-intro-background" alt="Responsive image">
                <div class="intro-box-text-left">
                    <h1 class="text-left sarabun pt-xl-3 pt-lg-4 pl-md-3 pt-md-3 pl-1 pt-3 pl-3 intro-text-header"><i>ค รี ม โ ส ม ส มุ น ไ พ ร</i></h1>
                    <li class="text-left intro-text sarabun pr-lg-5 mr-lg-5 pr-md-5 pl-md-2 pl-lg-3">
                        ครีมโสมสมุนไพร <br>
                        สารสกัดจากสมุนไพรไทยดั้งเดิม รวมสมุนไพรตัวท็อป <br>
                        ประกอบด้วย โสมดำ มะหาด ไพล รากชะเอม <br>
                        และน้ำมันมะกอก มาพร้อมกลิ่นผ่อนคลาย <br>
                        ช่วยบำรุงผิวเพื่อผิวเปล่งประกาย ลดเลือนรอยดำ <br>
                        และขาวกระจ่างใสอย่างเป็นธรรมชาติ <br>
                    </li>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 pl-md-0 pr-sm-0 p-0">
                <div class="img-avocado-header-box">
                    <img src="{{asset('img/webp/28.webp')}}" class="img-avocado-header bt-480 lazyload" alt="Responsive image">
                </div>
                <img src="{{asset('img/webp/10.webp')}}" class="img-intro-product lazyload" alt="Responsive image">
            </div>
        </div>
    </div>
</section>
<section class="main-camomile pt-5 mt-5 margin-bottom-lg-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 pl-sm-0 pr-md-0 p-0">
                <div class="img-avocado-header-box">
                    <img src="{{asset('img/webp/29.webp')}}" class="img-avocado-header lazyload" alt="Responsive image">
                </div>
                <img src="{{asset('img/webp/12.webp')}}" class="img-intro-product lazyload" alt="Responsive image">
            </div>
            <div class="col-lg-7 col-md-7 p-0">
                <img src="{{asset('img/webp/11.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                <div class="intro-box-text-right">
                    <h1 class="text-right sarabun pt-xl-3 pr-xl-3 pt-lg-4 pt-md-3 pr-2 pt-3 intro-text-header"><i>น้ำ ต บ ค า โ ม ม า ย ล์</i></h1>
                    <li class="text-right intro-text sarabun pl-lg-5 ml-lg-5 pr-md-2 pl-md-5 pr-lg-3">
                        น้ำตบคาโมมายล์<br>
                        น้ำตบคาโมมายล์ x คาเลนดูล่า<br>
                        สารสกัดหลักจากดอกคาโมมายล์และคาเลนดูล่า<br>
                        พร้อมทั้งไฮยารูลอนเข้มข้น และวิตามินซีบริสุทธิ์<br>
                        ปลอบประโลมผิว เพื่อผิวแข็งแรงสุขภาพดี<br>
                        ยืนหนึ่งเรื่องสิวผด ผลิตมาเพื่อผิวแพ้ง่ายโดยเฉพาะ<br>
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="macadamia pt-5 mt-5 margin-bottom-lg-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 p-0">
                <img src="{{asset('img/webp/24.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                <div class="intro-box-text-left">
                    <h1 class="text-left sarabun pt-xl-3 pt-lg-4 pt-md-3 pl-xl-3 pl-md-2 pt-3 pl-3 intro-text-header"><i>มาร์กสครับแมคคาเดเมีย</i></h1>
                    <li class="text-left intro-text sarabun pr-lg-5 mr-lg-5 pr-md-5 pl-md-2 p-1 pl-lg-3">
                        สครับที่สามารถเป็นได้ทั้งมาส์กและสครับใน 1 เดียว<br>
                        มีเม็ดสครับเป็นเมล็ดวอลนัทอยู่ในตัว<br>
                        ผลัดเซลล์ผิวอย่างอ่อนโยน ช่วยฟื้นฟูผิวแห้งกร้าน<br>
                        ผิวขาดน้ำพร้อมเติมน้ำให้ผิว เผยผิวเนียนเด้งน่าสัมผัส<br>
                        ลดรอยสิวต่างๆ พร้อมทั้งกระชับรูขุมขนอีกด้วย<br>
                        ทำผิวให้เปียก นวดวนและทิ้งไว้ 10 นาที<br>
                    </li>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 pl-md-0 pr-sm-0 p-0">
                <div class="img-avocado-header-box">
                    <img src="{{asset('img/webp/26.webp')}}" class="img-avocado-header bt-480 lazyload" alt="Responsive image">
                </div>
                <img src="{{asset('img/webp/25.webp')}}" class="img-intro-product lazyload" alt="Responsive image">
            </div>
        </div>
    </div>
</section>

<!-- Login Modal -->
{{-- <div class="modal fade" id="loginModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header border-none ">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-box">
            <h1 class="modal-title border-none w-100 text-center font-weight-bold mb-3" id="staticBackdropLabel">
                <li><span class="title-darkgreen">LOGIN TO</span> <span class="title-green">MSGYB</span></li>
            </h1>
            <div class="d-flex justify-content-center mb-3">
                <div class="col-md-4"  style="border: 1px solid#7E866F;"></div>
            </div>
            <div class="container description-login">
                <span>Enter your information to <br> access your account</span>
            </div>
            <login-agent></login-agent>
        </div>
    </div>
    </div>
</div> --}}
@endsection
