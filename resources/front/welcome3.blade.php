{{-- ******************* --}
                {{-- avocado --}}
                <div class="col-md-1"><div></div></div>
                {{-- background green --}}
                <div class="col-md-10 bg-main bg-main-height-sec2 mt-md-60 mt-lg-120 lazyload" style="z-index: -1;"></div>
                {{-- <div class="col-md-1 bg-main bg-main-height-sec2 mt-120 lazyload" style="z-index: -1;"></div> --}}
                <div class="col-md-1"><div></div></div>
                {{-- padding หน้า คอนเท้น avo --}}
                <div class="col-md-1 padding-avo-1"><div></div></div>
                <div class="col-md-10 mt-120 lazyload padding-avo-1 avocado-box">
                    <div class="container p-md-0">
                        <div class="row" style="box-shadow: 5px 7px 7px #ACB1A8;">
                            <div class="col-md-5 col-lg-5 avocado-image p-md-0">
                                <img src="{{asset('img/webp/avo.webp')}}" class="avocado-image w-100 lazyload" alt="avocado">
                                <div class="avocado-listband sarabun">สครับอโวคาโด้</div>
                            </div>
                            <div class="col-md-7 col-lg-7 bg-white pl-md-0">
                                <h1 class="avocado-head sarabun pl-lg-60 omyim">Avocado</h1>
                                <h3 class="avocado-detail sarabun pl-lg-60 pl-md-30 pt-lg-30">
                                    สครับอโวคาโด้สูตรใหม่ เน้นในเรื่องของผิวขาวกระจ่างใส<br>
                                    ลดเลือนรอยดำ สมานผิวให้เรียบเนียน<br>
                                    ช่วยผลัดเซลล์ผิวอย่างอ่อนโยนโดยไม่ทำให้ผิวบาง<br>
                                    ผสานด้วยเมล็ดสครับจากเปลือกอโวคาโด้และเมล็ดคาเคา<br>
                                    เนื้อสครับนุ่มฟู ถูแล้วแตกตัวเป็นน้ำนม<br>
                                    สามารถขัดได้ทุกวัน โดยไม่ทำให้ผิวบาง
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 bg-main bg-main-height-sec2 padding-avo-2 mt-120 lazyload"><div></div></div>
                {{-- ******************* end avo ******************* --}}
                {{-- background-green-camomild --}}
                <div class="col-md-1 padding-bg-green-camo"><div></div></div>
                <div class="col-md-10 bg-main2 bg-main-height-sec3 lazyload padding-bg-green-camo" style="z-index: -1;"><div></div></div>
                <div class="col-md-1 padding-bg-green-camo"><div></div></div>
                {{-- ************************************ --}}
                {{-- camomild-content --}}
                <div class="col-md-1 padding-camo"><div></div></div>
                <div class="col-md-10 mt-120 lazyload padding-camo">
                    <div class="container p-md-0">
                        <div class="row" style="box-shadow: 5px 7px 7px #ACB1A8;">
                            <div class="col-md-6 col-lg-7 bg-white pl-md-0 pt-90">
                                <h1 class="avocado-head sarabun pl-60 omyim">น้ำตบคาโมมายล์</h1>
                                <h3 class="avocado-detail sarabun pl-60">
                                    น้ำตบคาโมมายล์ x คาเลนดูล่า<br>
                                    สารสกัดหลักจากดอกคาโมมายล์และคาเลนดูล่า<br>
                                    พร้อมทั้งไฮยารูลอนเข้มข้น และวิตามินซีบริสุทธิ์<br>
                                    ปลอบประโลมผิว เพื่อผิวแข็งแรงสุขภาพดี<br>
                                    ยืนหนึ่งเรื่องสิวผด ผลิตมาเพื่อผิวแพ้ง่ายโดยเฉพาะ<br>
                                </h3>
                            </div>
                            <div class="col-md-6 col-lg-5 avocado-image p-md-0">
                                <img src="{{asset('img/webp/camo.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                                <div class="camo-listband sarabun">น้ำตบคาโมมายล์</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 padding-camo"><div></div></div>
                {{-- ************************************ --}}
                {{-- มาร์กสครับแมคคาเดเมีย --}}
                <div class="col-md-1 padding-camo"><div></div></div>
                <div class="col-md-10 mt-120 lazyload padding-camo">
                    <div class="container p-md-0">
                        <div class="row" style="box-shadow: 5px 7px 7px #ACB1A8;">
                            <div class="col-md-6 col-lg-7 pl-md-0">
                                <h1 class="avocado-head sarabun pl-60 omyim">มาร์กสครับแมคคาเดเมีย</h1>
                                <h3 class="avocado-detail sarabun pl-60 pt-30">
                                    สครับที่สามารถเป็นได้ทั้งมาส์กและสครับใน 1 เดียว<br>
                                    มีเม็ดสครับเป็นเมล็ดวอลนัทอยู่ในตัว<br>
                                    ผลัดเซลล์ผิวอย่างอ่อนโยน ช่วยฟื้นฟูผิวแห้งกร้าน<br>
                                    ผิวขาดน้ำพร้อมเติมน้ำให้ผิว เผยผิวเนียนเด้งน่าสัมผัส<br>
                                    ลดรอยสิวต่างๆ พร้อมทั้งกระชับรูขุมขนอีกด้วย<br>
                                    ทำผิวให้เปียก นวดวนและทิ้งไว้ 10 นาที<br>
                                </h3>
                            </div>
                            <div class="col-md-6 col-lg-5 avocado-image p-md-0">
                                <img src="{{asset('img/webp/cacao.webp')}}" class="img-intro-background lazyload" alt="Responsive image">
                                <div class="camo-listband sarabun">Avocao X Cacao</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 padding-camo"><div></div></div>
                {{-- ********************************* --}}
                <div class="col-md-2"></div>
                {{-- image cacao --}}
        </div>
    </div>
    <div class="container-fluid footer-image lazyload"></div>
