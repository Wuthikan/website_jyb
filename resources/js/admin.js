window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue';
import VueTouch from 'vue-touch';
import Trend from 'vuetrend';

import Toasted from 'vue-toasted';
import VueApexCharts from 'vue-apexcharts';
import VueCookies from 'vue-cookies';

import store from './admin/store';
import router from './admin/Routes';
import App from './admin/App';
import layoutMixin from './admin/mixins/layout';
import Form from './Form';
import VueEvents from 'vue-events';
//conponents
import Loader from './admin/components/loader/Loader';
import Widget from './admin/components/widget/Widget';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlus, faEdit, faTrashAlt, faUndo, faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Swal from 'vue-sweetalert2';
import Vuetable from 'vuetable-2';
import {VuetablePagination, VuetablePaginationInfo, VuetablePaginationDropDown, VuetableFieldHandle} from 'vuetable-2';
//page
import LoginPage from './admin/login/Login.vue';

window.Form = Form;
window.axios = require('axios');
Vue.use(BootstrapVue);
Vue.use(VueEvents);
Vue.use(VueTouch);
Vue.use(Trend);
Vue.component('Loader', Loader);
Vue.component('Widget', Widget);
Vue.component('apexchart', VueApexCharts);
library.add(faPlus, faEdit, faTrashAlt, faUndo, faBars)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.mixin(layoutMixin);
Vue.use(Toasted, {duration: 5000});
Vue.use(VueCookies);
Vue.use(Swal);
Vue.component('vuetable', Vuetable);
Vue.component("vuetable-pagination", VuetablePagination);
Vue.component("vuetable-pagination-dropdown", VuetablePaginationDropDown);
Vue.component("vuetable-pagination-info", VuetablePaginationInfo);
Vue.component('VuetableFieldHandle', VuetableFieldHandle);

Vue.config.productionTip = false;


if (document.getElementById('app-login')) {
    const page_login = new Vue({
        el: '#app-login',
        store,
        components: {
            'login-page': LoginPage
        }
    })
}

if (document.getElementById('app')) {
    const page_layout = new Vue({
        el: '#app',
        store,
        router,
        render: h => h(App),
    })
}
