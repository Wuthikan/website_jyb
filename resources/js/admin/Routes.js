import Vue from 'vue';
import Router from 'vue-router';

import Layout from './layout/Layout';
import ErrorPage from './error/Error';
import LoginPage from './login/Login';
// Main
import AnalyticsPage from './pages/Dashboard/Dashboard';

import MemberAll from './pages/Member/MemberAll';
import Brand from './pages/setting/brand/Brand';
import BrandCreate from './pages/setting/brand/BrandCreate';
import BrandTrash from './pages/setting/brand/BrandTrash';
import Price from './pages/setting/price/Price';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/error',
      name: 'Error',
      component: ErrorPage,
    },
    {
      path: '/login',
      name: 'Login',
      component: LoginPage,
    },
    {
      path: '/',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'dashboard',
          name: 'AnalyticsPage',
          component: AnalyticsPage,
        },
        {
          path: 'member',
          name: 'member',
          children: [
            {
              path: 'all',
              name: 'MemberAll',
              component: MemberAll,
            }
          ]
        },
        {
          path: 'setting/brand',
          name: 'setting/brand',
          component: Brand
        },
        {
          path: 'setting/brand/create',
          name: 'setting/brand/create',
          component: BrandCreate
        },
        { path: 'setting/brand/:id',
          name: 'setting/brand/edit',
          component: BrandCreate 
        },
        { path: 'setting/trash/brand',
          name: 'setting/trash/brand',
          component: BrandTrash 
        },
        {
          path: 'setting/price',
          name: 'setting/price',
          component: Price
        }
      ],
    },
    { path: "*", component: ErrorPage }
  ],
});
