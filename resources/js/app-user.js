window.Vue = require('vue');
window.axios = require('axios');
import BootstrapVue from 'bootstrap-vue';

import Form from './Form';
//Fontawesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faHeart, faPhoneAlt, faPhoneSquareAlt, faIdCard, faLink } from '@fortawesome/free-solid-svg-icons';
import { faLine, faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueToastr from 'vue-toastr';
import {func} from './front/js/global.js';
import VueYouTubeEmbed from 'vue-youtube-embed';

Vue.use(VueYouTubeEmbed);

Vue.use(BootstrapVue);
window.Form = Form;

//Use Fontawesome
library.add( faHeart, faPhoneAlt, faPhoneSquareAlt, faLine, faFacebookSquare, faIdCard, faLink);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueToastr, {
    /* OverWrite Plugin Options if you need */
});
Vue.prototype.$func = func;

import TopDroupdownProduct from './front/components/TopDroupdownProduct.vue';

//page
import RegisterBrand from './front/pages/registerBrands/RegisterBrandComponent';  //หน้าสมัครตัวแทนแบรนด์
import RegisterBrandWait from './front/pages/registerBrands/RegisterBrandWait';  //หน้ารอแม่ทีมยืนยัน
import ApproveUser from './front/pages/team/approveUsers.vue'; //หน้ารับลูกทีม
import PhotoBrand from './front/pages/photoBrand.vue'; //หน้ารูป
import VdoTrain from './front/pages/VdoTrain.vue';  //หน้าวิดิโอ
import ShowListVdoTrain from './front/pages/ShowListVdo.vue';  //หน้าลีสวิดิโอ
import UserProfile from './front/pages/user/userProfile.vue'; //หน้าโปรไฟล์ Auth 
import ChangePassword from './front/pages/user/changePassword.vue';
import EditUserProfile from './front/pages/user/EditUserProfile.vue'; //หน้าแก้ไขโปรไฟล์ Auth 
import MemberCard from './front/pages/MemberCard.vue'; //บัตรตัวแทน
import TotalMember from './front/pages/team/TotalMember.vue'; //สายงานทั้งหมดของแต่ละไอดี

if (document.getElementById('navbar-user')) {
    const navbar_user = new Vue({
        el: '#navbar-user',
        components: {
            'top-droupdown-product': TopDroupdownProduct
        }
    })
}

if (document.getElementById('page-brand-register')) {
    const brand_register = new Vue({
        el: '#page-brand-register',
        components: {
            'register-brand': RegisterBrand
        }
    })
}


if (document.getElementById('page-register-products-wait')) {
    const brand_register_wait = new Vue({
        el: '#page-register-products-wait',
        components: {
            'register-brand-wait': RegisterBrandWait
        }
    })
}

if (document.getElementById('approve-user-register')) {
    const approve_users = new Vue({
        el: '#approve-user-register',
        components: {
            'approve-users': ApproveUser
        }
    })
}

// รูปภาพ
if (document.getElementById('photo-brand')) {
    const approve_users = new Vue({
        el: '#photo-brand',
        components: {
            'photo-brand': PhotoBrand
        }
    })
}

// user profile
if (document.getElementById('user-profile')) {
    const approve_users = new Vue({
        el: '#user-profile',
        components: {
            'user-profile': UserProfile
        }
    })
}

if (document.getElementById('change-password')) {
    const approve_users = new Vue({
        el: '#change-password',
        components: {
            'change-password': ChangePassword
        }
    })
}

// show list vdo train
if (document.getElementById('show-list-vdo-train')) {
    const approve_users = new Vue({
        el: '#show-list-vdo-train',
        components: {
            'show-list-vdo-train': ShowListVdoTrain
        }
    })
}

// vdo train
if (document.getElementById('vdo-train')) {
    const approve_users = new Vue({
        el: '#vdo-train',
        components: {
            'vdo-train': VdoTrain
        }
    })
}

if (document.getElementById('edit-user-profile')) {
    const edit_users_prodile = new Vue({
        el: '#edit-user-profile',
        components: {
            'edit-user-profile': EditUserProfile
        }
    })
}

// บัตรตัวแทน
if (document.getElementById('member-card-user')) {
    const member_card_user = new Vue({
        el: '#member-card-user',
        components: {
            'member-card-user': MemberCard
        }
    })
}

// สายงานทั้งหมด
if (document.getElementById('total-member')) {
    const total_member = new Vue({
        el: '#total-member',
        components: {
            'total-member': TotalMember
        }
    })
}
