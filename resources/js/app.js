
require('./bootstrap');
// const { $, jQuery } = require('jquery');

window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue';
import Vuelidate from 'vuelidate';
import VueRouter from 'vue-router'
import Vuetable from 'vuetable-2';
import VueGoodTablePlugin from 'vue-good-table';
// import VueMobileDetection from 'vue-mobile-detection',
// import VuetablePagination from 'vue-table-pagination'


var Vue = require('vue');
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.component('vuetable', Vuetable);
Vue.use(VueGoodTablePlugin, 'vue-good-table');
// Vue.use(VueMobileDetection)
// Vue.use(VuetablePagination);

import Form from './Form';
// import RegisterComponent from './front/pages/RegisterComponent.vue';
// import MemberComponent from './front/pages/MemberComponent.vue';

window.Form = Form;

Vue.component('member-total', require('./front/pages/MemberComponent.vue').default);
Vue.component('detail-member', require('./front/pages/DetailMemberComponent.vue').default);
Vue.component('register-agent', require('./front/pages/RegisterComponent.vue').default);
Vue.component('login-agent', require('./front/pages/LoginComponent.vue').default);
Vue.component('navbar-main', require('./front/pages/NavbarComponent.vue').default);
Vue.component('show-product', require('./front/pages/ProductComponent.vue').default);
Vue.component('rating-products', require('./front/pages/RatingProductsComponent.vue').default);
Vue.component('about-brand', require('./front/pages/AboutBrandComponent.vue').default);
Vue.component('contact-us', require('./front/pages/ContactComponent.vue').default);
Vue.component('card-navbar', require('./front/pages/CardUserNavbarComponent.vue').default);

Vue.component('disabled-button', require('./front/components/disabledButton.vue').default);
// Vue.component('approve-users', require('./front/pages/approveUsers.vue').default);
// Vue.component('member-card', require('./front/pages/memberCardComponent.vue').default);


const app = new Vue({
    el: '#app',
});

// if(document.getElementById('register')) {
//     const app = new Vue({
//         el: '#register',
//         components: {
//             'register-agent' : RegisterComponent
//         }
//     });
// }


