<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Models\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('login', function() {
//     abort(401);
// })->name('login');

// Route::post('login', function () {
//     $credentials = request()->only(['email', 'password']);

//     if (!auth()->validate($credentials)){
//         abort(401);
//     } else {
//         $user = User::where('email', $credentials['email'])->first();
//         $role = $user->role;
//         $user->tokens()->delete();
//         $token = $user->createToken('postman', [$role]);
//         return response()->json(['token' => $token->plainTextToken]);
//     }
// });

// Route::group(['middleware' => ['auth:sanctum']], function(){
//     Route::get('numbers', function() {
//         $user = auth()->user();
//         if($user->tokenCan('admin') || $user->tokenCan('super_admin')){
//             return response()->json([1,2,3,4,5]);
//         }else {
//             abort(401);
//         }
//     });
// });