<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','WelcomeController@index');
// Route::get('/home-test','WelcomeController@testhome');

// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/login', function () {
    return redirect('/');
});
Route::post('login', 'Auth\LoginController@login');
Route::any('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('forgot-password', 'Auth\ForgotPasswordController@index')->name('forgot-password');
//Auth have middleware
Route::post('api-change-password', 'Auth\ChangePasswordController@store')->name('api-change-password');
Route::get('api-profile-user', 'Auth\ProfileController@show')->name('api-profile-user');
Route::patch('api-profile-user/profile/{id}', 'Auth\ProfileController@updateProfile')->name('api-profile-user.update');
Route::patch('api-profile-user/social/{id}', 'Auth\ProfileController@updateSocial')->name('api-profile-user.update.social');
Route::patch('api-profile-user/address/{id}', 'Auth\ProfileController@updateAddress')->name('api-profile-user.update.address');

Route::resource('contract', 'ContractController');
Route::get('register', 'RegisterController@create')->name('register');
Route::post('register', 'RegisterController@store');
Route::get('member', 'front\MemberController@index')->name('member');
Route::get('detail-member/{id}', 'front\DetailMemberController@show')->name('detail-member');
Route::get('about-brand', 'front\AboutBrandController@index')->name('about-brand');
Route::get('contact', 'front\ContactController@index')->name('contact');
Route::get('product/{slugname}', 'front\ProductController@index')->name('product');

// Profile
Route::middleware('auth')->namespace('front')->group(function () {
    Route::get('profile', 'user\UserController@profile')->name('user');
    Route::get('profile/edit', 'user\UserController@editProfile')->name('user-edit');
    Route::get('profile/change-password', 'user\UserController@changePassword')->name('profile.change-password');
});
// When Login
Route::middleware('auth')->namespace('front\products')->group(function () {
    //open brands
    Route::get('products', 'OpenBrandController@index')->name('products');
    Route::get('{slugname}/{idbrand}/register', 'OpenBrandController@register')->name('{slugname}.{idbrand}.register');
    Route::post('register-brand', 'OpenBrandController@store')->name('register-brand');
    //dashboard
    Route::get('{slugname}/{idbrand}/dashboard', 'DashboardController@index')->name('{slugname}.{idbrand}.dashboard');
    //สายงาน
    Route::get('{slugname}/{idbrand}/team', 'team\TeamController@index')->name('{slugname}.{idbrand}.team');
    Route::get('team/api/{idbrand}/{userid}', 'team\TeamController@showListUsers')->name('{slugname}.{idbrand}.team.api');
    Route::get('{slugname}/{idbrand}/team/waiting', 'team\TeamController@indexWaiting')->name('{slugname}.{idbrand}.team.waiting');
    //รูปภาพ
    Route::get('{slugname}/{idbrand}/photos', 'photos\PhotosController@index')->name('{slugname}.{idbrand}.photos');
    Route::get('photos/api/{idbrand}', 'photos\PhotosController@apiPhoto')->name('{slugname}.{idbrand}.photos');
    //วิดิโอ
    Route::get('{slugname}/{idbrand}/videos', 'vdoes\VdoesController@index')->name('{slugname}.{idbrand}.vdoes');
    Route::get('{slugname}/{idbrand}/videos/{id}', 'vdoes\VdoesController@show')->name('{slugname}.{idbrand}.vdoes.{id}');
    Route::get('videos/api/{idbrand}', 'vdoes\VdoesController@apiVdos')->name('{slugname}.{idbrand}.vdoes');
    Route::get('videos/api/{idbrand}/list/{id}', 'vdoes\VdoesController@apiVdosList')->name('{slugname}.{idbrand}.vdoes');
});
// When Login
Route::prefix('api-user')->name('api-user')->middleware('auth')->group(function () {
    // Page Register Member Products
    Route::get('get-advisors', 'front\MemberController@getAdviser')->name('get-advisors');
    Route::get('get-price-rate/{brand}', 'front\price\PriceRateController@getPriceRate')->name('get-price-rate');

    //test
    // Route::get('member-card', 'front\memberCardController@index')->name('member-card');
});

Route::prefix('api-front')->name('api-front')->namespace('api')->group(function () {
    Route::get('navbar', 'NavbarController@index')->name('navbar');
    Route::get('rating', 'RatingProductsController@index')->name('rating');
    Route::get('carduser', 'CardUserNavbarController@index')->name('carduser');
    Route::get('api-users', 'isApiApproveUsersController@index')->name('api-users');
    //get data register User to approve
    Route::get('api-user-register/{brand}/{id}', 'isApiApproveUsersController@getDataIsRegisterUser')->name('api-user-register');
    Route::post('update-aprrove-user/', 'isApiApproveUsersController@updateApproveUser')->name('update-aprrove-user'); //approve user
    Route::delete('delete-approve-user/{id}', 'isApiApproveUsersController@deleteUserApprove')->name('delete-aprrove-user'); //delete approve user
});



//admin

Route::prefix('admin')->name('admin.')->namespace('admin')->group(function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::group(['middleware' => ['admin']], function(){

        Route::prefix('api')->name('api.')->namespace('api')->group(function () {
            //brand
            Route::get('brand/trash', 'BrandController@trash')->name('brand.trash');
            Route::get('brand/restore/{id}', 'BrandController@restore')->name('brand.restore');
            Route::delete('brand/force-delete/{id}', 'BrandController@forceDelete')->name('brand.forceDelete');
            Route::apiResource('brand', 'BrandController');
            //price
            Route::apiResource('price', 'PriceController');
        });
    });
});

//image path
Route::get('image/public/{database}/{id}/{filename}', 'DisplayImageController@imagePublic')->name('image.public');
Route::get('image/global/{database}/{id}/{filename}', 'DisplayImageController@imageGlobal')->name('image.global');
Route::get('image/private/{database}/{id}/{filename}', 'DisplayImageController@imagePrivate')->name('image.private')->middleware('auth');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/listView', function () {
    return view('listView');
});






