const mix = require('laravel-mix');
// require('laravel-mix-purgecss');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
   //Fake Image Logo Product
   .copyDirectory('public/img/logo-product/avocado.webp', 'storage/app/images/global/brands/1')
   .copyDirectory('public/img/logo-product/thaiherbalbody.webp', 'storage/app/images/global/brands/2')
   .copyDirectory('public/img/logo-product/chamomile.webp', 'storage/app/images/global/brands/3')
   //Fake Image Brands
   .copyDirectory('public/img/rate/avocado', 'storage/app/images/global/brands/1') //avocado
   .copyDirectory('public/img/rate/thai-herbal', 'storage/app/images/global/brands/2') //thai-herbal
   .copyDirectory('public/img/rate/chamomile', 'storage/app/images/global/brands/3') //chamomile
   //Fake Image Photo
   .copyDirectory('public/img/rate/avocado/2.jpg', 'storage/app/images/global/photo/1')
   .copyDirectory('public/img/rate/avocado/3.jpg', 'storage/app/images/global/photo/2')
   .copyDirectory('public/img/rate/chamomile/2.jpg', 'storage/app/images/global/photo/3')
   .copyDirectory('public/img/rate/chamomile/3.jpg', 'storage/app/images/global/photo/4')
   .copyDirectory('public/img/rate/thai-herbal/2.jpg', 'storage/app/images/global/photo/5')
   .copyDirectory('public/img/rate/thai-herbal/3.jpg', 'storage/app/images/global/photo/6')
   //Fake Image Card
   .copyDirectory('public/img/cards/avocado.webp', 'storage/app/images/global/cards/1')
   .copyDirectory('public/img/cards/thai-herbal.webp', 'storage/app/images/global/cards/2')
   .copyDirectory('public/img/cards/chamomile.webp', 'storage/app/images/global/cards/3')
   //Fake Image Users
   .copyDirectory('public/img/examples/profile/1.jpg', 'storage/app/images/global/users/1')
   .copyDirectory('public/img/examples/profile/2.jpg', 'storage/app/images/global/users/2')
   .copyDirectory('public/img/examples/profile/3.jpg', 'storage/app/images/global/users/3')
   .copyDirectory('public/img/examples/profile/4.jpg', 'storage/app/images/global/users/4')
   .copyDirectory('public/img/examples/profile/5.jpg', 'storage/app/images/global/users/5')
   .copyDirectory('public/img/examples/profile/6.jpg', 'storage/app/images/global/users/6')
   .copyDirectory('public/img/examples/profile/7.jpg', 'storage/app/images/global/users/7')
   .copyDirectory('public/img/examples/profile/8.jpg', 'storage/app/images/global/users/8')
   .copyDirectory('public/img/examples/profile/9.jpg', 'storage/app/images/global/users/9')
   .copyDirectory('public/img/examples/profile/10.jpg', 'storage/app/images/global/users/10')
   .copyDirectory('public/img/examples/profile/11.jpg', 'storage/app/images/global/users/11')
   .copyDirectory('public/img/examples/profile/12.jpg', 'storage/app/images/global/users/12')
   .copyDirectory('public/img/examples/profile/13.jpg', 'storage/app/images/global/users/13')
   .copyDirectory('public/img/examples/profile/14.jpg', 'storage/app/images/global/users/14')
   .copyDirectory('public/img/examples/profile/15.jpg', 'storage/app/images/global/users/15')
   .copyDirectory('public/img/examples/profile/16.jpg', 'storage/app/images/global/users/16')
   .copyDirectory('public/img/examples/profile/17.jpg', 'storage/app/images/global/users/17')
   //Fake Image qrcodeline
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/1')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/2')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/3')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/4')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/5')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/6')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/7')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/8')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/9')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/10')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/11')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/12')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/13')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/14')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/15')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/16')
   .copyDirectory('public/img/examples/users/qrcodeline.jpg', 'storage/app/images/global/users/17')
   //Fake Image Idcard
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/1')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/2')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/3')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/4')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/5')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/6')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/7')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/8')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/9')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/10')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/11')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/12')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/13')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/14')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/15')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/16')
   .copyDirectory('public/img/examples/users/idcard.jpg', 'storage/app/images/private/users/17')

   .js('resources/js/app.js', 'public/js')
   .sass('resources/sass/front.scss', 'public/css')

   .js('resources/js/app-user.js', 'public/js')
   .sass('resources/sass/front-user.scss', 'public/css')

   .js('resources/js/admin.js', 'public/js/admin')
   .sass('resources/sass/admin.scss', 'public/css');

